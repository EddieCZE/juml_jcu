package cz.jcu.juml.gui.others;

import com.sun.javafx.tk.Toolkit;

import cz.jcu.juml.gui.parts.GUIComment;
import javafx.geometry.Pos;
import javafx.scene.control.TextField;
import javafx.scene.input.ClipboardContent;
import javafx.scene.input.Dragboard;
import javafx.scene.input.KeyCode;
import javafx.scene.input.TransferMode;

@SuppressWarnings("restriction")
/**
 * Custom TextField to react on specific actions:
 * * focus gain
 * * width computing
 * * DnD listeners
 * @author Adam Chovanec
 *
 */
public class InputTextField extends TextField {

	private static final int TEXTFIELD_PREFIX_WIDTH = 15; //25
	private static final String defaultRegex = ".*";

	private String regex;

	/**
	 * Managing size, aligment and properties of text field
	 * 
	 * @param promptText replacement text
	 */
	public InputTextField(String promptText) {
		regex = defaultRegex;

		setAlignment(Pos.CENTER);
		setPromptText(promptText);
		computeWidthToTextLength(promptText);
		setEventHandlers();
	}
	/**
	 * Set of regular expressions allowed to use in text field
	 * Used in Datatype text fields to highlight different kinds of data types
	 * @param regex
	 */
	public void setRegex(String regex){
		this.regex = regex;
	}

	private void setEventHandlers(){		
		textProperty().addListener((observable, oldText, newText) -> {
			if(newText.length() == 0 || newText.matches(regex)){
				setText(newText);
				computeWidthToTextLength(newText);
			} else {
				setText(oldText);
			}
		});

		focusedProperty().addListener((observable, lostFocus, hasFocus) -> {
				if(getText().length() == 0 && lostFocus)
					computeWidthToTextLength(getPromptText());
		});

		setOnKeyPressed(e -> {
			System.out.println(this.getParent());
			System.out.println(!(this.getParent() instanceof GUIComment));
			if(e.getCode() == KeyCode.ENTER && !(this.getParent() instanceof GUIComment))
				this.setFocused(false);
		});
		
		setOnDragDetected(e -> {
			if(!(this.getParent() instanceof GUIComment)){
				Dragboard db = startDragAndDrop(TransferMode.ANY);
				ClipboardContent content = new ClipboardContent();
				content.putString(getText());
				db.setContent(content);
				e.consume();
			}
			e.consume();
		});
	}

	private void computeWidthToTextLength(String text){	
		float width = TEXTFIELD_PREFIX_WIDTH + Toolkit.getToolkit().getFontLoader().computeStringWidth(text, getFont());
		setMinWidth(width);
		setMaxWidth(width);
		setPrefWidth(width);
	}
}
