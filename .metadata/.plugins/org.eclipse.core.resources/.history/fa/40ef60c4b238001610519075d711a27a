package cz.jcu.juml.logic.javaspec.parts;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import cz.jcu.juml.logic.javaspec.AccessModifier;
import cz.jcu.juml.logic.javaspec.EntityType;
import cz.jcu.juml.logic.javaspec.NonAccessModifier;
import cz.jcu.juml.logic.others.LogicUtils;
import javafx.beans.property.SimpleStringProperty;

public final class Entity extends Element implements Serializable {

	private static final long serialVersionUID = 1L;
	
	//basic definition
	Pckage pckage;
	final EntityType entityType;
	final List<String> importedEntities;
	final List<Attribute> attributes;
	final List<Method> constructors;
	final List<Method> methods;
    final List<Method> enums;

	//inheritance definition
	Entity parent; //if extends, then this is parent
	final List<Entity> implementedEntities;
	AccessModifier accessModAsNested;
	final List<Entity> nestedEntities;

	public Entity(AccessModifier accessMod, List<NonAccessModifier> nonAccessMods, SimpleStringProperty identifier,
			Pckage pckage, EntityType entityType, List<String> importedEntities,
			List<Attribute> attributes, List<Method> constructors, List<Method> methods,
                        List<Method> enums, Entity parent, List<Entity> implementedEntities, AccessModifier accessModAsNested, List<Entity> nestedEntities) {
		super(accessMod, nonAccessMods, identifier);
		this.pckage = pckage;
		this.entityType = entityType;
		this.importedEntities = importedEntities;
		this.attributes = attributes;
		this.constructors = constructors;
		this.methods = methods;
                this.enums=enums;

		this.parent = parent;
		this.implementedEntities = implementedEntities;
		this.accessModAsNested = accessModAsNested;
		this.nestedEntities = nestedEntities;
	}

	//getters
	public Pckage getPckage() {
		return pckage;
	}

	public EntityType getEntityType() {
		return entityType;
	}

	public List<String> getImportedEntities() {
		return importedEntities;
	}

	public List<Attribute> getAttributes() {
		return attributes;
	}

	public List<Method> getConstructors() {
		return constructors;
	}

	public List<Method> getMethods() {
		return methods;
	}
        
	public List<Method> getEnums(){
		return enums;
	}

	public Entity getParent() {
		return parent;
	}

	public List<Entity> getImplementedEntities() {
		return implementedEntities;
	}

	public AccessModifier getAccessModAsNested() {
		return accessModAsNested;
	}

	public List<Entity> getNestedEntities() {
		return nestedEntities;
	}

	public boolean isNested() {
		return true;//dod�lat
	}

	public boolean isNestedStatic() {
		return false;//dod�lat
	}

	//setters
	public void setPckage(Pckage pckage) {
		this.pckage = pckage;
	}

	public void setParent(Entity parent) {
		this.parent = parent;
	}

	public void setAccessModAsNested(AccessModifier accessModAsNested) {
		this.accessModAsNested = accessModAsNested;
	}
	
	//copy constructor
	public static Entity newInstance(Entity entity) {
		return new Entity(
				entity.accessMod,
				new ArrayList<NonAccessModifier>(entity.nonAccessMods),
				new SimpleStringProperty(entity.identifier.getValue()),
				entity.pckage,
				entity.entityType,
				new ArrayList<String>(entity.importedEntities),
				new ArrayList<Attribute>(entity.attributes), //Utils
				new ArrayList<Method>(entity.constructors), //Utils
				new ArrayList<Method>(entity.methods), //Utils
                                new ArrayList<Method>(entity.enums), //Utils
				entity.parent,
				new ArrayList<Entity>(entity.implementedEntities), //Utils
				entity.accessModAsNested,
				new ArrayList<Entity>(entity.nestedEntities) //Utils
				);
	}

	//overwritten methods
	@Override
	public String toJavaCode() {
		return LogicUtils.toJavaCode(
				accessMod,
				LogicUtils.listToJavaCode("","", nonAccessMods, " ", ""),
				entityType,
				identifier.getValue(),
				"{\n",
				//v��ty
				LogicUtils.listToJavaCode("\n", LogicUtils.tab(), enums, ",\n", ";\n"),
				//Attributy
				LogicUtils.listToJavaCode("\n", LogicUtils.tab(), attributes, "\n", "\n"),
				//Konstruktory
				LogicUtils.listToJavaCode("\n", LogicUtils.tab(), constructors, "\n", ""),
				//Metody
				LogicUtils.listToJavaCode("\n", LogicUtils.tab(), methods, "\n", ""),
				"\n}");
	}

	@Override
	public String toString() {
		return super.toString() + ", entityType: " + entityType + ", numOfAttr: " + attributes.size() + ", numOfCons: " + constructors.size() + ", numOfMethods: " + methods.size() + ", numOfEnums: " + enums.size() + ", parentDetected: " + (parent != null);
	}

	//TODO upravit equals
	@Override
	public boolean equals(Object o) {
		if (o == null) {
			return false;
		}
		if (o == this) {
			return true;
		}
		if (!(o instanceof Entity)) {
			return false;
		}
		Entity obj = (Entity) o;
		return super.equals(obj)
				&& this.entityType == obj.entityType
				&& this.attributes.equals(obj.attributes)
				&& this.constructors.equals(obj.constructors)
				&& this.methods.equals(obj.methods)
				&& this.parent.equals(obj.parent);
	}

	//static methods
	public static Method getMainMethod(Entity entity){
		for(Method method : entity.methods){
			if(method.isPublic() && method.isStatic() && !method.isFinal() && !method.isAbstract()
					&& method.type.getValue().equals("void") && method.identifier.getValue().equals("main")
					&& method.parameters.size() == 1 && method.parameters.get(0).type.equals("String")
					&& method.parameters.get(0).isArray()){

				return method;    			    			
			}
		}
		return null;
	}

}
