package cz.jcu.juml.gui.others;

import java.util.Collection;
import java.util.Collections;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.fxmisc.richtext.CodeArea;
import org.fxmisc.richtext.StyleSpans;
import org.fxmisc.richtext.StyleSpansBuilder;

import cz.jcu.juml.assets.AssetsProvider;
import javafx.scene.control.ContextMenu;
import javafx.scene.control.Menu;
import javafx.scene.control.MenuItem;
import javafx.scene.layout.AnchorPane;

/**
 * Custom java code editor extends CodeArea to allow user to edit or write their own source code
 * This class slightly extends source code of JavaKeywords.java obtained from Tomas Mikula Rich Text demos
 * Adding menu items for generating Java language structures
 * Different syntax highlighting for primitives data types and identifiers 
 * 
 * 
 * 
 * @author Tom� Mikula
 * @author Adam Chovanec
 *
 */

public class JavaCodeEditor extends CodeArea {

        private MenuItem ifCondition = new MenuItem("if");
        private MenuItem ifElseCondition = new MenuItem("if/else");
        private MenuItem whileCycle = new MenuItem("while");
        private MenuItem forCycle = new MenuItem("for");
        private MenuItem forEachCycle = new MenuItem("for each");
        private MenuItem rowComment = new MenuItem("koment�� jednoho ��dku");
        private MenuItem blockComment = new MenuItem("blokov� koment��");
        private MenuItem thisInit = new MenuItem("Inicializace t��dy - this");
        private MenuItem thisConstruct = new MenuItem("Nad�azen� konstruktor");
        private MenuItem superConstruct = new MenuItem("Konstruktor p�edka");



        private Menu languageStructureMenu = new Menu("Jazykov� struktury");
        private Menu callingMenu = new Menu("Vol�n� t��d a konstruktor�");
        private Menu commentsMenu = new Menu("Koment��e");
        private String currentText=this.getText();
        public ContextMenu editorsStructures=new ContextMenu();
    
            
	private static final String[] KEYWORDS = new String[] {
		"abstract", "assert", "boolean", "break", "byte",
		"case", "catch", "class", "const",
		"continue", "char", "default", "do", "double", "else",
		"enum", "extends", "final", "finally", "float",
		"for", "goto", "if", "implements", "import", "int",
		"instanceof", "interface", "long", "native",
		"new", "package", "private", "protected", "public",
		"return", "static", "strictfp", "super", "short",
		"switch", "synchronized", "this", "throw", "throws",
		"transient", "try", "void", "volatile", "while"
	};

	private static final String[] PRIMITIVES = new String[] {
		"byte", "short", "int", "long",
		"float", "double",
		"char",
		"boolean"
	};
	
	
	private static final String KEYWORD_PATTERN = "\\b(" + String.join("|", KEYWORDS) + ")\\b";
	private static final String PAREN_PATTERN = "\\(|\\)";
	private static final String BRACE_PATTERN = "\\{|\\}";
	private static final String BRACKET_PATTERN = "\\[|\\]";
	private static final String SEMICOLON_PATTERN = "\\;";
	private static final String STRING_PATTERN = "\"([^\"\\\\]|\\\\.)*\"";
	private static final String COMMENT_PATTERN = "//[^\n]*" + "|" + "/\\*(.|\\R)*?\\*/";
	private static final String GENERICS_PATTERN = "\\<|\\>";
	private static final String REFERENCE_PATTERN = "[A-Z]+[a-zA-Z0-9]*";
	
	
	private static final Pattern PATTERN = Pattern.compile(
			"(?<KEYWORD>" + KEYWORD_PATTERN + ")"
					+ "|(?<PAREN>" + PAREN_PATTERN + ")"
					+ "|(?<BRACE>" + BRACE_PATTERN + ")"
					+ "|(?<BRACKET>" + BRACKET_PATTERN + ")"
					+ "|(?<SEMICOLON>" + SEMICOLON_PATTERN + ")"
					+ "|(?<STRING>" + STRING_PATTERN + ")"
					+ "|(?<COMMENT>" + COMMENT_PATTERN + ")"
					+ "|(?<GENERICS>" + GENERICS_PATTERN + ")"
					+ "|(?<REFERENCE>" + REFERENCE_PATTERN + ")"
			);

	public static final String sampleCode = String.join("\n", new String[] {
			"package com.example;",
			"",
			"import java.util.*;",
			"",
			"public class Test {",
			" ",
			"    public static void main(String[] args) {"
	});
	//
        
        public ContextMenu initEditorStuctures(){
            //Language structures
            ifCondition.setOnAction(e->{
                this.insertText("if(/*podm�nka*/){\n\n}");
            });
            ifElseCondition.setOnAction(e->{
                this.insertText("if(podm�nka){\n\n}\nelse{\n\n}");
            });
            whileCycle.setOnAction(e->{
                this.insertText("while(podm�nka){\n\n}");
            });
            forCycle.setOnAction(e->{
                this.insertText("for(int i=0;i>0;i++){\n\n}");
            });
            forEachCycle.setOnAction(e->{
                this.insertText("for(Object o : Iterator<E>){\n\n}");
            });
            //Comments structures
            rowComment.setOnAction(e->{
                this.insertText("//��dkov� koment��");
            });
            blockComment.setOnAction(e->{
                this.insertText("/* \n *blokov� koment�� \n */");
            });
            //Callings
            thisInit.setOnAction(e->{
                this.insertText("this.");
            });
            thisConstruct.setOnAction(e->{
                this.insertText("this(zadej spr�vn� po�ad� parametr� uvozeno ��rkou)");
            });
            superConstruct.setOnAction(e->{
                this.insertText("super(zadej spr�vn� po�ad� parametr� uvozeno ��rkou)");
            });
            languageStructureMenu.getItems().addAll(ifCondition,ifElseCondition,whileCycle,forCycle,forEachCycle);
            commentsMenu.getItems().addAll(rowComment,blockComment);
            callingMenu.getItems().addAll(thisInit,thisConstruct,superConstruct);
            editorsStructures.getItems().addAll(callingMenu,languageStructureMenu,commentsMenu);
            return editorsStructures;
        }
        
        
        
        private void insertText(String text){
            String currText = GUIManager.currentMethod.bodyProperty().getValue();
            String newText = new StringBuilder(currText).insert(getCaretPosition(), text).toString();
            int pom = getCaretPosition();
            replaceText(newText);
            positionCaret(pom + text.length());
            GUIManager.currentMethod.bodyProperty().setValue(newText);
        }
       /*
        public void setCurrentText(String newText){
            this.currentText=newText;
        }
        */
        public String getCurrentText(){
            return this.currentText;
        }
	
        public JavaCodeEditor(String code){
        	AnchorPane.setTopAnchor(this, 0.0);
        	AnchorPane.setRightAnchor(this, 0.0);
        	AnchorPane.setLeftAnchor(this, 0.0);
        	AnchorPane.setBottomAnchor(this, 0.0);
		
		getStylesheets().add("cz/jcu/juml/" + AssetsProvider.pathToCSS("java-keywords.css"));
		
		textProperty().addListener((obs, oldText, newText) -> {
			setStyleSpans(0, computeHighlighting(newText));
		});
		replaceText(0, 0, code);
	}

        /**
         * Real-time java syntax highlighting according to common convention
         * regex is break apart 
         * 
         * @param text to be highlighted
         * @return
         */
	public static StyleSpans<Collection<String>> computeHighlighting(String text) {
		Matcher matcher = PATTERN.matcher(text);
		int lastKwEnd = 0;
		StyleSpansBuilder<Collection<String>> spansBuilder
		= new StyleSpansBuilder<>();
		while(matcher.find()) {
			String styleClass = matcher.group("KEYWORD") !=null ? "keyword" :
								matcher.group("PAREN") != null ? "paren" :
								matcher.group("BRACE") != null ? "brace" :
								matcher.group("BRACKET") != null ? "bracket" :
								matcher.group("SEMICOLON") != null ? "semicolon" :
								matcher.group("STRING") != null ? "string" :
								matcher.group("COMMENT") != null ? "comment" :
								matcher.group("GENERICS") != null ? "generics" : null;

			String word = text.substring(matcher.start(), matcher.end());
			
			if(matcher.group("KEYWORD") != null){
				styleClass = "keyword";
				for(String primitive : PRIMITIVES){
					if(word.equals(primitive)){
						styleClass = "primitive";
						break;
					}
				}
			}
			
			if(matcher.group("REFERENCE") != null)
				styleClass = "reference";
			
			spansBuilder.add(Collections.emptyList(), matcher.start() - lastKwEnd);
			spansBuilder.add(Collections.singleton(styleClass), matcher.end() - matcher.start());
			lastKwEnd = matcher.end();
		}
		spansBuilder.add(Collections.emptyList(), text.length() - lastKwEnd);
		return spansBuilder.create();
	}
}
