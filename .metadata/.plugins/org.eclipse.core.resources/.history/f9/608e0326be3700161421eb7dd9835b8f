package cz.jcu.juml.gui.others;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import cz.jcu.juml.gui.controllers.GUIController;
import cz.jcu.juml.gui.controllers.GUIEditorController;
import cz.jcu.juml.gui.parts.GUIComment;
import cz.jcu.juml.gui.parts.GUIEntity;
import cz.jcu.juml.logic.javaspec.parts.Entity;
import cz.jcu.juml.logic.javaspec.parts.Method;
import cz.jcu.juml.logic.javaspec.parts.Part;
import cz.jcu.juml.logic.others.Comment;
import cz.jcu.juml.logic.others.EntityManager;
import cz.jcu.juml.logic.others.io.IOUtils;
import cz.jcu.juml.logic.others.io.Serializer;
import javafx.collections.ObservableList;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.effect.GaussianBlur;
import javafx.scene.layout.Pane;
import javafx.stage.FileChooser;
import javafx.stage.Stage;

public class GUIManager {


	
	//private static final List<GUIEntity> guiEntities = new ArrayList<GUIEntity>();

	public static GUIController guiController;
    public static GUIEditorController guiEditorController;
	public static Stage mainStage;
	public static Stage pathsSettingsDialog;
	public static Stage syntaxHighlightDialog;
	public static Stage aboutDialog;
    public static Stage editorStage;
    public static Method currentMethod;
    public static final List<Comment> comments = new ArrayList<Comment>();
    
	
	public static void showDialog(Stage stage, Parent root){
		GUIManager.guiController.mainBorderPane.setEffect(new GaussianBlur(5.0));
		stage.setScene(new Scene(root));
		stage.show();
	}
	public static void registerGUIController(GUIController newGuiController){
		GUIManager.guiController = newGuiController;
	}
	
	public static void registerMainStage(Stage stage){
		GUIManager.mainStage = stage;
	}

	public static GUIEntity getGUIEntity(Entity entity){
		ObservableList<Node> entities = guiController.projectDesignerPane.getChildren();
		for(Node node : entities){
			if(node instanceof GUIEntity){
				GUIEntity guiEntity = (GUIEntity) node;
				if(guiEntity.logicEntity.equals(entity))
					return guiEntity;
			}
		}
		return null;
	}
	
	public static GUIComment getGUIComment(Comment comment){
		ObservableList<Node> comments = guiController.projectDesignerPane.getChildren();
		for(Node node : comments){
			if(node instanceof GUIComment){
				GUIComment guiComment = (GUIComment) node;
				if(guiComment.getComment().equals(comment))
					return guiComment;
			}
		}
		return null;
		
	}

	public static GUIEntity createGUIEntity(Entity logicEntity){
		GUIEntity guiEntity = new GUIEntity(logicEntity, guiController.projectDesignerPane);
		guiEntity.scaleXProperty().bind(guiController.sliderZoom.valueProperty());
		guiEntity.scaleYProperty().bind(guiController.sliderZoom.valueProperty());
				
		guiController.projectDesignerPane.getChildren().add(guiEntity);
		return guiEntity;
	}
	
	public static GUIComment createGUIComment(Comment logicComment){
		GUIComment guiComment = new GUIComment(logicComment, guiController.projectDesignerPane);
		guiComment.scaleXProperty().bind(guiController.sliderZoom.valueProperty());
		guiComment.scaleYProperty().bind(guiController.sliderZoom.valueProperty());
				
		guiController.projectDesignerPane.getChildren().add(guiComment);
		return guiComment;
		
	}
	
	public static void add(Part logicPart, Pane guiParent){
	}

	public static void removeAllEntities(){
		guiController.projectDesignerPane.getChildren().clear();
		EntityManager.removeAllEntities();
	}
	
	public static void saveProject(){
        try {
            FileChooser fileChooser = new FileChooser();
            fileChooser.getExtensionFilters().add(new FileChooser.ExtensionFilter("soubor jUML (*.juml)", "*.juml"));
            fileChooser.setInitialDirectory(new File(IOUtils.workspacePath));
            GUIManager.guiController.mainBorderPane.setEffect(GUIUtils.GAUSSIAN_BLUR);
            File file = fileChooser.showSaveDialog(GUIManager.mainStage);

            if (file != null) {
                Serializer.serializeProject(file);
            }
            GUIManager.guiController.mainBorderPane.setEffect(null);
        } catch (Exception ex) {
        	AlertProvider.errorAlert("P�i ukl�d�n� projektu se vyskytla neo�ek�van� chba", "Chyba p�i ukl�d�n� projektu");
        }
    }
	
	public static void loadProject(){
        try {
            FileChooser fileChooser = new FileChooser();
            fileChooser.getExtensionFilters().add(new FileChooser.ExtensionFilter("soubor jUML (*.juml)", "*.juml"));
            fileChooser.setInitialDirectory(new File(IOUtils.workspacePath));
            GUIManager.guiController.mainBorderPane.setEffect(GUIUtils.GAUSSIAN_BLUR);
            File file = fileChooser.showOpenDialog(GUIManager.mainStage);
            GUIManager.guiController.mainBorderPane.setEffect(null);
            if (file != null) {
                GUIManager.removeAllEntities();
                Serializer.deserializeProject(file);
            }
        } catch (Exception e1) {
            AlertProvider.errorAlert("P�i na��t�n� projektu se vyskytla neo�ek�van� chba", "Chyba p�i na��t�n� projektu");
        }
    }
}