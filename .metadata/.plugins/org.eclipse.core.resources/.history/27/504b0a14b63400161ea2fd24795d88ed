package cz.jcu.juml.logic.others;

import java.io.Serializable;

import javafx.beans.property.SimpleStringProperty;

public class Comment implements Serializable {

	private static final long serialVersionUID = 1L;
	
	private transient SimpleStringProperty header;
	private transient SimpleStringProperty content;
	
	public Comment(String header, String content) {
		this.header = new SimpleStringProperty(header);
		this.content = new SimpleStringProperty(content);
	}
	
	public void setHeader(String header) {
		this.header.setValue(header);
	}
	
	public void setContent(String content) {
		this.content.setValue(content);
	}
	
	public SimpleStringProperty getHeader() {
		return this.header;
	}
	
	public SimpleStringProperty getContent() {
		return this.content;
	}

	@Override
	public boolean equals(Object o) {
		if (o == null) {
			return false;
		}
		if (o == this) {
			return true;
		}
		if (!(o instanceof Comment)) {
			return false;
		}
		Comment obj = (Comment) o;
		return this.header.getValue().equals(obj.header.getValue()) && this.content.getValue().equals(obj.content.getValue());
	}
}
