package cz.jcu.juml.logic.javaspec.parts;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import cz.jcu.juml.logic.javaspec.AccessModifier;
import cz.jcu.juml.logic.javaspec.NonAccessModifier;
import cz.jcu.juml.logic.others.LogicUtils;
import javafx.beans.property.SimpleStringProperty;

public final class Method extends Member implements Serializable {

    private static final long serialVersionUID = 1L;

    List<Parameter> parameters;
    transient SimpleStringProperty body;
    transient SimpleStringProperty comment;
    boolean isConstructor;
    boolean isEnum;

    public Method(Entity parent, boolean isConstructor, boolean isEnum) {//this out, must be solved (gui problem)
        this(AccessModifier.PACKAGE_PRIVATE, new ArrayList<NonAccessModifier>(), new SimpleStringProperty(""), new SimpleStringProperty(""), new ArrayList<Parameter>(), new SimpleStringProperty(""), new SimpleStringProperty(""), isConstructor, isEnum, parent);
    }

    public Method(AccessModifier accessMod, List<NonAccessModifier> nonAccessMods, SimpleStringProperty type, SimpleStringProperty identifier, List<Parameter> parameters, SimpleStringProperty comment, SimpleStringProperty body, boolean isConstructor, boolean isEnum, Entity parent) {
        super(accessMod, nonAccessMods, identifier, type, parent);
        this.parameters = parameters;
        this.body = body;
        this.comment = comment;
        this.isConstructor = isConstructor;
        this.isEnum=isEnum;

        if (isConstructor) {
            identifierProperty().bind(getParent().identifierProperty());
        }
                        //System.out.println(this.type.getValue().equals(DataType.VOID.getPrefix()));

    }

    public List<Parameter> getParameters() {
        return parameters;
    }

    public SimpleStringProperty bodyProperty() {
        return body;
    }

    public SimpleStringProperty commentProperty() {
        return comment;
    }

    public boolean isConstructor() {
        return isConstructor;
    }
    
    public boolean isEnum(){
        return isEnum;
    }

    public void setBody(SimpleStringProperty body) {
        this.body = body;
    }

    public void setComment(SimpleStringProperty comment) {
        this.comment = comment;
    }

    public String getHeader() {
        return LogicUtils.toJavaCode(
                accessMod,
                LogicUtils.listToJavaCode("", "", nonAccessMods, " ", ""),
                type.getValue(),
                identifier.getValue(),
                LogicUtils.listToJavaCode("(","",parameters, ", ", ")"),
                "{");
    }

    @Override
    public String toJavaCode() {
        String pom;
        if (this.isEnum) {
            pom = LogicUtils.toJavaCode(identifier.getValue(), parameters.isEmpty()?"":LogicUtils.listToJavaCode("(","",parameters, ", ", ")"));
        } else {
            pom = LogicUtils.toJavaCode(getHeader(),"\n",body.getValue(),"\n", "}\n");
        }        
        return pom;
    }

    @Override
    public String toString() {
        return super.toString() + ", numOfParams: " + parameters.size() + ", body: " + body;
    }

    @Override
    public boolean equals(Object o) {
        if (o == null) {
            return false;
        }
        if (o == this) {
            return true;
        }
        if (!(o instanceof Method)) {
            return false;
        }
        Method obj = (Method) o;
        return super.equals(obj)
                && this.parameters.equals(obj.parameters)
                && this.body.equals(obj.body);
    }
}
