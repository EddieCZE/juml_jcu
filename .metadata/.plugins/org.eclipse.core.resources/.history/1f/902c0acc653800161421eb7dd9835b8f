package cz.jcu.juml.gui.others;

import com.sun.javafx.tk.Toolkit;

import cz.jcu.juml.gui.parts.GUIComment;
import javafx.scene.control.TextArea;
import javafx.scene.input.ClipboardContent;
import javafx.scene.input.Dragboard;
import javafx.scene.input.KeyCode;
import javafx.scene.input.TransferMode;

@SuppressWarnings("restriction")
/**
 * Custom TextArea to react on specific actions:
 * * focus gain
 * * width computing
 * * DnD listeners
 * @author Adam Chovanec
 *
 */
public class InputTextArea extends TextArea {

	private static final int TEXTFIELD_PREFIX_WIDTH = 15; //25
	/**
	 * Managing size and properties of text area
	 * 
	 * @param promptText replacement text
	 */
	public InputTextArea(String promptText) {
			
		setPromptText(promptText);
		computeWidthToTextLength(promptText);
		setEventHandlers();
		
		setMinSize(100, 50);
		setMaxSize(300, 300);
		setWrapText(true);
	}

	private void setEventHandlers(){		
		textProperty().addListener((observable, oldText, newText) -> {
			setText(newText);
			computeWidthToTextLength(newText);
		});

		focusedProperty().addListener((observable, lostFocus, hasFocus) -> {
				if(getText().length() == 0 && lostFocus)
					computeWidthToTextLength(getPromptText());
		});

		setOnKeyPressed(e -> {
			System.out.print(e.getSource() instanceof GUIComment);
			if(e.getCode() == KeyCode.ENTER && !(e.getSource() instanceof GUIComment))
				this.setFocused(false);
		});
		
		setOnDragDetected(e -> {
			if(!(e.getSource() instanceof GUIComment)){
			Dragboard db = startDragAndDrop(TransferMode.ANY);
			ClipboardContent content = new ClipboardContent();
			content.putString(getText());
			db.setContent(content);
			e.consume();
			}
			e.consume();
		});
	}

	private void computeWidthToTextLength(String text){	
		float width = TEXTFIELD_PREFIX_WIDTH + Toolkit.getToolkit().getFontLoader().computeStringWidth(text, getFont());
		setPrefWidth(width);
	}
}
