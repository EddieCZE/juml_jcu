package cz.jcu.juml;

import java.util.Optional;

import cz.jcu.juml.assets.AssetsProvider;
import cz.jcu.juml.gui.others.AlertProvider;
import cz.jcu.juml.gui.others.GUIManager;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import javafx.scene.image.Image;
import javafx.stage.Stage;

/**
 * This is main class which is responsible for launching an application.
 * Application is written in JavaFX so it extends from javafx.application.Application.
 *
 * Use main method to launch application.
 */
public class GUIRun extends Application {
    
	//application version
	private static final String VERSION = "v0.8";
	//application window minimum width
	private static final int MIN_WIDTH = 1024;
	//application window minimum height
	private static final int MIN_HEIGHT = 768;
	//application window title
	private static final String APP_TITLE = "jUML Designing Tool" + " " + VERSION;
	
	/**
	 * Overridden method start defining application window and gathering all
	 * necessary files (fxmls, controllers, css, images).
	 */
    @Override
    public void start(Stage stage) {
    	try {
	    	Parent root = FXMLLoader.load(GUIRun.class.getResource(AssetsProvider.pathToFXML("GUI.fxml")));
	    	Scene scene = new Scene(root);
	        scene.getStylesheets().add(getClass().getResource(AssetsProvider.pathToCSS("application.css")).toExternalForm());
	        stage.setMinWidth(MIN_WIDTH);
	        stage.setMinHeight(MIN_HEIGHT);
	        stage.setScene(scene);
	        stage.setTitle(APP_TITLE);
	        stage.getIcons().add(new Image(AssetsProvider.pathToImage("icon2.png")));
	        stage.show();
	        stage.setOnCloseRequest(e->{
	       	Alert closeAlert = AlertProvider.exitWarningConfirm("Opravdu chcete ukon�it program? V�echny neulo�en� data budou ztracena", "Po�adavek na ukon�en� programu");
	        Optional<ButtonType> result=closeAlert.showAndWait();
	        System.out.print(result.get());
			if(result.get() == ButtonType.OK){
				System.exit(1);
			}
			else{
				closeAlert.close();
				e.consume();
			}
	        });
	        GUIManager.registerMainStage(stage);
	        GUIManager.pathsSettingsDialog.show();
    	} catch (Exception e) {
    		AlertProvider.exceptionAlert(e).showAndWait();
    	} 
    }

    /**
     * Main method of project. Launches application
     * @param args
     */
    public static void main(String[] args) {
        launch(args);
    }
    
}
