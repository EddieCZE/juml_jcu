package cz.jcu.juml.gui.controllers;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.URL;
import java.util.Optional;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.fxmisc.richtext.LineNumberFactory;
import cz.jcu.juml.GUIRun;
import cz.jcu.juml.assets.AssetsProvider;
import cz.jcu.juml.gui.others.AlertProvider;
import cz.jcu.juml.gui.others.GUIManager;
import cz.jcu.juml.gui.others.GUIUtils;
import cz.jcu.juml.gui.others.JavaCodeEditor;
import cz.jcu.juml.gui.parts.GUIComment;
import cz.jcu.juml.gui.parts.GUIEntity;
import cz.jcu.juml.logic.javaspec.EntityType;
import cz.jcu.juml.logic.javaspec.parts.Entity;
import cz.jcu.juml.logic.others.Comment;
import cz.jcu.juml.logic.others.EntityManager;
import cz.jcu.juml.logic.others.io.Compilerr;
import cz.jcu.juml.logic.others.io.IOUtils;
import cz.jcu.juml.logic.others.io.JarExporter;
import cz.jcu.juml.logic.others.io.Serializer;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.geometry.Orientation;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.CheckMenuItem;
import javafx.scene.control.Label;
import javafx.scene.control.MenuItem;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.Slider;
import javafx.scene.control.SplitPane;
import javafx.scene.control.TextArea;
import javafx.scene.image.ImageView;
import javafx.scene.input.ClipboardContent;
import javafx.scene.input.Dragboard;
import javafx.scene.input.TransferMode;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.VBox;
import javafx.stage.FileChooser;

/**
 * FXML controller responsible for initialization GUI elements and Java code editors. 
 * Creates Java code editors.
 * Sets listeners for menu items buttons 
 * 
 * @author Adam Chovanec
 *
 */

public class GUIController extends AnchorPane implements Initializable {

    @FXML
    public BorderPane mainBorderPane;
    @FXML
    public AnchorPane javaCodeEditorPane;
    @FXML
    public AnchorPane projectDesignerPane;
    @FXML
    public AnchorPane errorCompilePane;
    @FXML
    public ScrollPane scrollPane;
    @FXML
    public SplitPane splitPane;
    @FXML
    public Slider sliderZoom;

    @FXML
    public Button btnNewInterface;
    @FXML
    public Button btnNewEnumeration;
    @FXML
    public Button btnNewClass;
    @FXML
    public Button btnNewAbstractClass;
    @FXML
    public ImageView btnNewComment1;
    
    @FXML
    public MenuItem menuItemNewProject;
    @FXML
    public MenuItem menuItemSaveProject;
    @FXML
    public MenuItem menuItemLoadProject;
    @FXML
    public MenuItem menuItemGenerateSourceFiles;
    @FXML
    public MenuItem menuItemPublishProject;

    @FXML
    public MenuItem menuItemCompileAll;

    @FXML
    public MenuItem menuItemPathsSettings;
    @FXML
    public MenuItem menuItemAbout;
    @FXML
    public CheckMenuItem menuItemOrientation;
    @FXML
    public TextArea txtErrors;

    public JavaCodeEditor javaCodeEditor0 = new JavaCodeEditor("//koment�� metody");
    public JavaCodeEditor javaCodeEditor1 = new JavaCodeEditor("public static void ukazka(String param1){");
    public JavaCodeEditor javaCodeEditor2 = new JavaCodeEditor("");
    public Label endMethod = new Label("}");

    //constructor
    /**
     * Method for registering this type of controller to responsible manager
     */
    public GUIController() {
        GUIManager.registerGUIController(this);
    }

    //methods
   
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        scrollPane.getStyleClass().add("project_designer_scroll-pane");
        //na�ten� dal��ch fxml
        try {
            Parent root = FXMLLoader.load(GUIRun.class.getResource(AssetsProvider.pathToFXML("GUIPathsSettingsDialog.fxml")));
            GUIManager.pathsSettingsDialog = GUIUtils.initCustomDialog("Nastaven� cest k pracovn�mu prost�ed� a kompil�toru");
            GUIManager.pathsSettingsDialog.setScene(new Scene(root));

            root = FXMLLoader.load(GUIRun.class.getResource(AssetsProvider.pathToFXML("GUIEditor.fxml")));
            GUIManager.editorStage = GUIUtils.initCustomDialog("Editovan� t��da");
            GUIManager.editorStage.setScene(new Scene(root));
            
            root = FXMLLoader.load(GUIRun.class.getResource(AssetsProvider.pathToFXML("GUIAbout.fxml")));
            GUIManager.aboutDialog = GUIUtils.initCustomDialog("O aplikaci");
            GUIManager.aboutDialog.setScene(new Scene(root));
            


        } catch (IOException e2) {
            e2.printStackTrace(); //TODO HEZKY DIALOG HERE
        }

        //file menu items
        menuItemNewProject.setOnAction(e -> {
        	Alert confirmAlert=new Alert(AlertType.CONFIRMATION);
        	confirmAlert.setTitle("Souhlas");
        	confirmAlert.setHeaderText("Vytvo�en� nov�ho projektu");
        	confirmAlert.setContentText("Opravdu chcete vytvo�it nov� projekt?");
        	Optional<ButtonType> result = confirmAlert.showAndWait();
        	if(result.get()== ButtonType.OK){
        		//vymaz�n� v�ech entit a zresetov�n� jejich po�tu
        		GUIManager.removeAllEntities();
        		EntityManager.setCounter(0);
        		resetEditor();
        	}
        	else{
        		confirmAlert.close();	
        	}
        });

        menuItemSaveProject.setOnAction(e -> {
        	GUIManager.saveProject();
        });

        menuItemLoadProject.setOnAction(e -> {
        	GUIManager.loadProject();
        });

        menuItemGenerateSourceFiles.setOnAction(e -> {
        	if (this.projectDesignerPane.getChildren().size() > 0) {
	            try {
	                File directory = IOUtils.createDirectory(IOUtils.workspacePath + "\\zdrojove_soubory");
	                IOUtils.entitiesAsJavaFiles(directory);
	                AlertProvider.GeneretionInformationAlert().show();
	
	            } catch (Exception ex) {
	                AlertProvider.GeneretionFailed("P�i generov�n� zdrojov�ho k�du nastala chyba!").show();
	            }
        	} else {
        		AlertProvider.GeneretionFailed("V projektu nem�te vytvo�enou ani jednu entitu.").show();
        	}
        });

        menuItemPublishProject.setOnAction(e -> {
            try {
                FileChooser fileChooser = new FileChooser();
                fileChooser.setInitialDirectory(new File(IOUtils.workspacePath));
                fileChooser.getExtensionFilters().add(new FileChooser.ExtensionFilter("soubor Java Archive (*.jar)", "*.jar"));
                GUIManager.guiController.mainBorderPane.setEffect(GUIUtils.GAUSSIAN_BLUR);
                File javaArchive = fileChooser.showSaveDialog(GUIManager.mainStage);
                GUIManager.guiController.mainBorderPane.setEffect(null);

                if (javaArchive != null) {
                    File directory = IOUtils.createDirectory(IOUtils.workspacePath + "\\zkompilovane_entity");
                    File[] files = Compilerr.compileAllEntities(directory);
                    JarExporter.createJarArchive(javaArchive, files, "");
                }
            } catch (Exception ex) {
                ex.printStackTrace();// TODO TADY NAKEJ HEZKEJ DIALOG
            }
        });

        //settings menu items
        menuItemOrientation.setSelected(false);
        splitPane.setOrientation(Orientation.HORIZONTAL);
        menuItemOrientation.selectedProperty().addListener((observable, oldValue, newValue) -> {
            splitPane.setOrientation(newValue ? Orientation.VERTICAL : Orientation.HORIZONTAL);
        });

        menuItemPathsSettings.setOnAction(e -> {
            GUIManager.pathsSettingsDialog.show();
        });

        menuItemCompileAll.setOnAction(e -> {
            File directory = IOUtils.createDirectory(IOUtils.workspacePath + "\\zkompilovane_entity");
            ObservableList<Node> nodes = GUIManager.guiController.projectDesignerPane.getChildren();
            String[] fileNames = new String[nodes.size()];
            String[] fileContents = new String[nodes.size()];
            for (int i = 0; i < nodes.size(); i++) {
                GUIEntity guiEntity = (GUIEntity) nodes.get(i);
                fileNames[i] = guiEntity.logicEntity.identifierProperty().getValue();
                fileContents[i] = guiEntity.logicEntity.toJavaCode();
            }
            try {
                boolean compilationResult = Compilerr.compileMultipleJavaClasses(directory, fileNames, fileContents);
                if(compilationResult){
                    txtErrors.setText("Kompilace prob�hla �sp�n�");
                }
                else{
                    txtErrors.setText(IOUtils.readFile(Compilerr.errorOutputFile).toString());
                }
            } catch (FileNotFoundException ex) {
                Logger.getLogger(GUIController.class.getName()).log(Level.SEVERE, null, ex);
            } catch (ClassNotFoundException ex) {
                Logger.getLogger(GUIController.class.getName()).log(Level.SEVERE, null, ex);
            } catch (IOException ex) {
                Logger.getLogger(GUIController.class.getName()).log(Level.SEVERE, null, ex);
            }

        });
        
        menuItemAbout.setOnAction(e->{
        	GUIManager.aboutDialog.show();
        	
        });

        //project designer pane
        setEntityButtonsDnD(btnNewClass, EntityType.CLASS);
        setEntityButtonsDnD(btnNewAbstractClass, EntityType.ABSTRACT_CLASS);
        setEntityButtonsDnD(btnNewEnumeration, EntityType.ENUMERATION);
        setEntityButtonsDnD(btnNewInterface, EntityType.INTERFACE);

        btnNewComment1.setOnDragDetected(e -> {
            Dragboard db = btnNewComment1.startDragAndDrop(TransferMode.ANY);
            ClipboardContent content = new ClipboardContent();
            content.putString("comment");
            db.setContent(content);
            e.consume();
        });
        
        projectDesignerPane.setOnDragOver(e -> {
            if (e.getGestureSource() != projectDesignerPane && e.getDragboard().getContent(GUIUtils.DATAFORMAT_ENTITY_TYPE) != null) {
                e.acceptTransferModes(TransferMode.MOVE);
            } else if (e.getGestureSource() != projectDesignerPane && e.getDragboard().getString() != null) {
            	e.acceptTransferModes(TransferMode.MOVE);
            }
            e.consume();
        });

        projectDesignerPane.setOnDragDropped(e -> {
            Dragboard db = e.getDragboard();
            
            Object draggedObject = db.getContent(GUIUtils.DATAFORMAT_ENTITY_TYPE);
            if (draggedObject != null && draggedObject instanceof EntityType) {
	            Entity logicEntity = EntityManager.createEntity((EntityType) db.getContent(GUIUtils.DATAFORMAT_ENTITY_TYPE));
	            GUIEntity guiEntity = GUIManager.createGUIEntity(logicEntity);
	
	            double startX = 0;
	            double startY = mainBorderPane.getTop().getBoundsInLocal().getHeight();
	            int offset = 5;
	            guiEntity.setLayoutX(e.getSceneX() - startX - offset);
	            guiEntity.setLayoutY(e.getSceneY() - startY - offset);
            } else {
            	draggedObject = db.getString();
            	if (draggedObject != null && draggedObject.equals("comment")){ 
            		Comment comment = new Comment("Nov� pozn�mka", "");
            		GUIManager.comments.add(comment);
            		GUIComment guiComment = new GUIComment(comment, projectDesignerPane);
            		double startX = 0;
     	            double startY = mainBorderPane.getTop().getBoundsInLocal().getHeight();
     	            int offset = 5;
     	            guiComment.setLayoutX(e.getSceneX() - startX - offset);
     	            guiComment.setLayoutY(e.getSceneY() - startY - offset);
     	            
     	            guiComment.scaleXProperty().bind(this.sliderZoom.valueProperty());
     	            guiComment.scaleYProperty().bind(this.sliderZoom.valueProperty());
	     	  				
     	           this.projectDesignerPane.getChildren().add(guiComment);
            	}
            }
            e.consume();
        });

        initJavaCodeEditors();
    }
    /**
     * Implements Drag&Drop function on graphic components
     * @param button to which the function will apply
     * @param entityType Type of entity that will be created
     */
    private void setEntityButtonsDnD(Button button, EntityType entityType) {
        button.setOnDragDetected(e -> {
            Dragboard db = button.startDragAndDrop(TransferMode.ANY);
            ClipboardContent content = new ClipboardContent();
            content.put(GUIUtils.DATAFORMAT_ENTITY_TYPE, entityType);
            db.setContent(content);
            e.consume();
        });
    }
    
    private void resetEditor(){
        javaCodeEditor0.replaceText("");
        javaCodeEditor1.replaceText("");
        javaCodeEditor2.replaceText("");
    }
/**
 * Prepare Java code editors on AnchorPane and sets listeners
 * Whole editing part consists of 3 editors
 * - Header: Reference from model declaration that is not editable
 * - Body: Actual source code editing
 * - Footer: Auto-ending brace of whole class
 */
    public void initJavaCodeEditors() {
        VBox codeEditors = new VBox();
        codeEditors.setStyle("-fx-background-color: white;");
        AnchorPane.setTopAnchor(codeEditors, 0.0);
        AnchorPane.setRightAnchor(codeEditors, 0.0);
        AnchorPane.setLeftAnchor(codeEditors, 0.0);
        javaCodeEditorPane.getChildren().add(codeEditors);
        javaCodeEditor1.setEditable(false);
        codeEditors.getChildren().add(javaCodeEditor1);
        javaCodeEditor1.setPrefHeight(19);
        javaCodeEditor2.setEditable(true);
        javaCodeEditor2.setParagraphGraphicFactory(LineNumberFactory.get(javaCodeEditor2));
        javaCodeEditor2.setPrefHeight(120);
        javaCodeEditor2.setContextMenu(javaCodeEditor2.initEditorStuctures());
        
        javaCodeEditor2.setOnKeyReleased(e -> {
            GUIManager.currentMethod.bodyProperty().setValue(javaCodeEditor2.textProperty().getValue());      
        });

        codeEditors.getChildren().add(javaCodeEditor2);
        codeEditors.getChildren().add(endMethod);

    }
}
