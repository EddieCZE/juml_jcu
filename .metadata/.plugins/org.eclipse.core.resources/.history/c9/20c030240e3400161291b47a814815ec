package cz.jcu.juml.gui.others;

import com.sun.javafx.tk.Toolkit;

import javafx.geometry.Pos;
import javafx.scene.control.TextArea;
import javafx.scene.input.ClipboardContent;
import javafx.scene.input.Dragboard;
import javafx.scene.input.KeyCode;
import javafx.scene.input.TransferMode;

@SuppressWarnings("restriction")
public class InputTextArea extends TextArea {

	private static final int TEXTFIELD_PREFIX_WIDTH = 15; //25
	
	public InputTextArea(String promptText) {
			
		setPromptText(promptText);
		computeWidthToTextLength(promptText);
		setEventHandlers();
		
		setMinSize(125, 50);
		setMaxSize(250, 300);
		setWrapText(true);
	}

	private void setEventHandlers(){		
		textProperty().addListener((observable, oldText, newText) -> {
			setText(newText);
			computeWidthToTextLength(newText);
		});

		focusedProperty().addListener((observable, lostFocus, hasFocus) -> {
				if(getText().length() == 0 && lostFocus)
					computeWidthToTextLength(getPromptText());
		});

		setOnKeyPressed(e -> {
			if(e.getCode() == KeyCode.ENTER)
				this.setFocused(false);
		});
		
		setOnDragDetected(e -> {
			Dragboard db = startDragAndDrop(TransferMode.ANY);
			ClipboardContent content = new ClipboardContent();
			content.putString(getText());
			db.setContent(content);
			e.consume();
		});
	}

	private void computeWidthToTextLength(String text){	
		float width = TEXTFIELD_PREFIX_WIDTH + Toolkit.getToolkit().getFontLoader().computeStringWidth(text, getFont());
		setPrefWidth(width);
	}
}
