package cz.jcu.juml.assets;

/**
 * This class is responsible for providing paths to assets files (fxmls, css, and images).
 * Library class design pattern is applied. 
 */
public final class AssetsProvider {
	
	private static final String FXML_PATH = "assets/fxmls/";
	private static final String CSS_PATH = "assets/css/";
	private static final String IMAGES_ABSOLUTE_PATH = "cz/jcu/juml/assets/images/";

	/**
	 * Private constructor
	 */
	private AssetsProvider() {}
	
	/**
	 * Returns path to FXML file from assets in application structure.
	 * @param fileName name of file to use, extension required
	 * @return Path to FXML file.
	 */
	public static String pathToFXML(String fileName) {
		return FXML_PATH + fileName;
	}
	
	/**
	 * Returns path to CSS file from assets in application structure.
	 * @param fileName name of file to use, extension required
	 * @return Path to CSS file.
	 */
	public static String pathToCSS(String fileName) {
		return CSS_PATH + fileName;
	}
	
	/**
	 * Returns absolute path to IMAGE file from assets in application structure.
	 * @param fileName name of file to use, extension required
	 * @return Path to IMAGE file.
	 */
	public static String pathToImage(String fileName) {
		return IMAGES_ABSOLUTE_PATH + fileName;
	}

}
