package cz.jcu.juml.logic.javaspec;

import cz.jcu.juml.logic.others.Codeable;

public enum DataType implements Codeable {

    BYTE("byte", "", "", true),
    SHORT("short", "", "", true),
    INT("int", "", "", true),
    LONG("long", "", "L", true),
    FLOAT("float", "", "f", true),
    DOUBLE("double", "", "d", true),
    BOOLEAN("boolean", "", "", true),
    CHAR("char", "'", "'", true),
    VOID("void", "" , "" ,true),
    STRING("String", "\"", "\"", false),
    OBJECT("Object", "new (", ")", false);
    

    private final String code;
    private final String prefix;
    private final String postfix;
    private final boolean isPrimitive;

    private DataType(String code, String prefix, String postfix, boolean isPrimitive) {
        this.code = code;
        this.prefix = prefix;
        this.postfix = postfix;
        this.isPrimitive = isPrimitive;
    }

    public String getPrefix() {
        return prefix;
    }

    public String getPostfix() {
        return postfix;
    }

    public Boolean isPrimitive() {
        return isPrimitive;
    }

    //Dirty solution
    public String getStringColor() {
        if (isPrimitive()) {
            return "red";
        } else if (code.equals("String")) {
            return "blue";
        } else {
            return "black";
        }
    }

    @Override
    public String toJavaCode() {
        return code;
    }
}
