package cz.jcu.juml.logic.javaspec;

import cz.jcu.juml.logic.others.Codeable;
import cz.jcu.juml.logic.others.Describable;

public enum NonAccessModifier implements Codeable, Describable {

	STATIC("static", "Statický"),
	FINAL("final", "Konečný"),
	ABSTRACT("abstract", "Abstraktní"),
	NONE("", "");
	
	private final String code;
	private final String description;
	
	private NonAccessModifier(String code, String description){
		this.code = code;
		this.description = description;
	}
	
	@Override
	public String toJavaCode() {
		return code;
	}

	@Override
	public String getDescription() {
		return description;
	}
}
