package cz.jcu.juml.logic.javaspec.parts;

import java.io.Serializable;
import java.util.List;

import javafx.beans.property.SimpleStringProperty;

public final class Pckage extends Part implements Serializable {

	private static final long serialVersionUID = 1L;
	
	transient final SimpleStringProperty identifier;
    final List<Entity> entities;
    //+ list bal��k�
    Pckage parent;

    public Pckage(SimpleStringProperty identifier, List<Entity> entities, Pckage parent) {
        this.identifier = identifier;
        this.entities = entities;
        this.parent = parent;
    }

    public SimpleStringProperty identifierProperty() {
        return identifier;
    }

    public List<Entity> getEntities() {
        return entities;
    }

    public Pckage getParent() {
        return parent;
    }

    @Override
    public String toJavaCode() {
        return "package " + identifier.getValue() + ";";
    }

    @Override
    public String toString() {
        return "identifier: " + identifier.getValue() + ", numOfEntities: " + entities.size() + ", parentDetected: " + (parent != null);
    }

    @Override
    public boolean equals(Object o) {
        if (o == null) {
            return false;
        }
        if (o == this) {
            return true;
        }
        if (!(o instanceof Pckage)) {
            return false;
        }
        Pckage obj = (Pckage) o;
        return super.equals(obj)
                && this.identifier.equals(obj.identifier)
                && this.entities.equals(obj.entities)
                && this.parent.equals(obj.parent);
    }
}
