package cz.jcu.juml.logic.others;

public interface Codeable {

	public String toJavaCode();
}
