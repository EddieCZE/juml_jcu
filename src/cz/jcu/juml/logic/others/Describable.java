package cz.jcu.juml.logic.others;

public interface Describable {
	
	public String getDescription();
}
