package cz.jcu.juml.logic.others;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * This class provides helper function for Logic part of application.
 * Mostly it handler string manipulation with any kind of Object or with
 * List<? extends Codeable> collection. Library class design pattern is applied.
 * 
 */
public final class LogicUtils {
	
	/**
	 * Private constructor
	 */
	private LogicUtils(){}

	/**
	 * Takes any kind of objects from variable argument and appending them into
	 * one big string. StringBuilder is used for better memory management.
	 * 
	 * Because array of codes can contain any kind of object, method checks from
	 * which instance comes every item in array and based on their type takes their String
	 * representation. For example if String is passed then toString() method is called.
	 * If object comes from entity implementing Codeable interface then toJavaCode().
	 * Method also handles Lists and traversing every item in it.
	 * Item is omitted in string result if does not comes from String, Codeable, or List.
	 * 
	 * @param codes array of any kind of items in varargs
	 * @return One big string composition of objects passed as argument. 
	 */
	public static StringBuilder toJavaCodeBuilder(Object... codes){
		StringBuilder sb = new StringBuilder();
		int pocet = codes.length;
		for(Object obj : codes){                      
			String text = "";
			if(obj instanceof String){
				text = obj.toString();
				pocet--;
			}
			else if(obj instanceof Codeable){
				text = ((Codeable) obj).toJavaCode();
				pocet--;
			}
			else if(obj instanceof List){
				text = "";
				@SuppressWarnings("unchecked")
				List<Codeable> list = (List<Codeable>)obj;
				Iterator<Codeable> it = list.iterator();
				while(it.hasNext()){
					sb.append("\t");
					sb.append(it.next().toJavaCode());
					sb.append("\n");
				}
			}
			sb.append(text);
			if(pocet>0 && !text.equals(""))
			//if(!text.equals("")) //TODO Solve this?
				sb.append(" ");
		}
		/*if(sb.charAt(sb.length() - 1) == ' ')
                    sb.delete(sb.length() - 1, sb.length());*/
		return sb;

	}
	
	/**
	 * Helper method taking any kind of objects from variable argument
	 * and passing them to toJavaCodeBuilder method, which results also returns.
	 * @param codes any kind of objects in varargs
	 * @return Result from
	 */
	public static String toJavaCode(Object... codes){
		return toJavaCodeBuilder(codes).toString();
	}

	/**
	 * Helper method taking items of <T> from variable argument and return them in new ArrayList.
	 * @param items any items of <T> in varargs
	 * @return new ArrayList with those items in it.
	 */
	@SafeVarargs
	public static <T> List<T> newList(T... items){
		List<T> newList = new ArrayList<T>();
		for(T item : items){
			newList.add(item);
		}		
		return newList; 
	}

	/**
	 * Helper method which takes any List<?> and transform it's content into string. StringBuilder is used for
	 * better memory management. Generic type of List interface have to extends from Codeable interface, because
	 * this method creates programming code from logic parts - and those has to be codeable.
	 * @param startfix string prepended at the beginning of final string result
	 * @param prefix string prepended before every List<?> item string representation
	 * @param listOfCodes List of Codeable items
	 * @param postfix string appended after every List<?> item string representation
	 * @param endfix string appended at the end of final string result 
	 * @return List of codeable items as string representing programming Java code.
	 */
	public static String listToJavaCode(String startfix, String prefix, List<? extends Codeable> listOfCodes, String postfix, String endfix){
		StringBuilder sb = new StringBuilder();
		Iterator<? extends Codeable> it = listOfCodes.iterator();
		sb.append(startfix);
		while(it.hasNext()){
			sb.append(prefix);
			sb.append(it.next().toJavaCode());
			if(it.hasNext())
				sb.append(postfix);
		}
		//sb.append(it.next().toJavaCode()); TODO solve this?
		sb.append(endfix);
		return sb.toString();		
	}

	/**
	 * Simple method simulates pressing TAB key on keyboard by inserting exactly three spaces.
	 * @return Three spaces as String.
	 */
	public static String tab(){
		return "   ";
	}
}
