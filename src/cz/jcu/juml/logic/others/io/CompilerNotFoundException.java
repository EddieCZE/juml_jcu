package cz.jcu.juml.logic.others.io;

import java.io.IOException;

public class CompilerNotFoundException extends IOException {
	
	private static final long serialVersionUID = 1L;

}
