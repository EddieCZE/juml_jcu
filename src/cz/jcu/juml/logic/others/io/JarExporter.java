package cz.jcu.juml.logic.others.io;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.jar.Attributes;
import java.util.jar.JarEntry;
import java.util.jar.JarOutputStream;
import java.util.jar.Manifest;

public class JarExporter {

	public static int BUFFER_SIZE = 10240;

	public static void createJarArchive(File archiveFile, File[] tobeJared, String mainClassName) throws IOException {

		byte buffer[] = new byte[BUFFER_SIZE];
		// Open archive file
		FileOutputStream stream = new FileOutputStream(archiveFile);

		Manifest manifest = new Manifest();
		manifest.getMainAttributes().put(Attributes.Name.MANIFEST_VERSION, "1.0");
		manifest.getMainAttributes().put(Attributes.Name.MAIN_CLASS, mainClassName);
		JarOutputStream out = new JarOutputStream(stream, manifest);
		
		for (int i = 0; i < tobeJared.length; i++) {
			if (tobeJared[i] == null || !tobeJared[i].exists()
					|| tobeJared[i].isDirectory())
				continue; // Just in case...

			// Add archive entry
			JarEntry jarAdd = new JarEntry(tobeJared[i].getName());
			jarAdd.setTime(tobeJared[i].lastModified());
			out.putNextEntry(jarAdd);

			// Write file to archive
			FileInputStream in = new FileInputStream(tobeJared[i]);
			while (true) {
				int nRead = in.read(buffer, 0, buffer.length);
				if (nRead <= 0)
					break;
				out.write(buffer, 0, nRead);
			}
			in.close();
		}

		out.close();
		stream.close();
	}

	public static void main(String[] args) throws IOException {
		File archiveFile = new File("E:\\eddiebp\\myapp.jar");
		archiveFile.createNewFile();
		File[] filesToArchive = new File[]{
				new File("E:\\eddiebp\\Entita1.class"),
				new File("E:\\eddiebp\\EntitaX1.class"),
				new File("E:\\eddiebp\\Tralaaaaa.class"),
				new File("E:\\eddiebp\\XXX.class"),
				new File("E:\\eddiebp\\MainTrida.class")
		};
		JarExporter.createJarArchive(archiveFile, filesToArchive, "MainTrida");
	}
}
