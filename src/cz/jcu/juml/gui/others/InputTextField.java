package cz.jcu.juml.gui.others;

import javafx.geometry.Pos;
import javafx.scene.control.TextField;
import javafx.scene.input.ClipboardContent;
import javafx.scene.input.Dragboard;
import javafx.scene.input.KeyCode;
import javafx.scene.input.TransferMode;

import com.sun.javafx.tk.Toolkit;

import cz.jcu.juml.gui.parts.GUIEntity;
import cz.jcu.juml.logic.javaspec.EntityType;
import cz.jcu.juml.logic.javaspec.parts.Entity;
import cz.jcu.juml.logic.others.EntityManager;

public class InputTextField extends TextField {

	private static final int TEXTFIELD_PREFIX_WIDTH = 15; //25
	private static final String defaultRegex = ".*";

	private String regex;

	public InputTextField(String promptText) {
		regex = defaultRegex;

		setAlignment(Pos.CENTER);
		setPromptText(promptText);
		computeWidthToTextLength(promptText);
		setEventHandlers();
	}

	public void setRegex(String regex){
		this.regex = regex;
	}

	private void setEventHandlers(){		
		textProperty().addListener((observable, oldText, newText) -> {
			if(newText.length() == 0 || newText.matches(regex)){
				setText(newText);
				computeWidthToTextLength(newText);
			} else {
				setText(oldText);
			}
		});

		focusedProperty().addListener((observable, lostFocus, hasFocus) -> {
				if(getText().length() == 0 && lostFocus)
					computeWidthToTextLength(getPromptText());
		});

		setOnKeyPressed(e -> {
			if(e.getCode() == KeyCode.ENTER)
				this.setFocused(false);
		});
		
		setOnDragDetected(e -> {
			Dragboard db = startDragAndDrop(TransferMode.ANY);
			ClipboardContent content = new ClipboardContent();
			content.putString(getText());
			db.setContent(content);
			e.consume();
		});
	}

	private void computeWidthToTextLength(String text){	
		float width = TEXTFIELD_PREFIX_WIDTH + Toolkit.getToolkit().getFontLoader().computeStringWidth(text, getFont());
		setMinWidth(width);
		setMaxWidth(width);
		setPrefWidth(width);
	}
}
