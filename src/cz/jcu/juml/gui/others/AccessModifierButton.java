package cz.jcu.juml.gui.others;

import java.util.List;

import cz.jcu.juml.logic.javaspec.AccessModifier;
import cz.jcu.juml.logic.javaspec.Restrictions;
import cz.jcu.juml.logic.javaspec.parts.Element;
import javafx.geometry.Insets;
import javafx.scene.control.Button;
import javafx.scene.control.Tooltip;
import javafx.scene.layout.HBox;

public final class AccessModifierButton extends Button {

	private static final int size = 16;//20;
	private static final Insets margins = new Insets(0, 0, 0, 5);
	private static final String CSS_BASIC_STYLECLASS = "access_modifier";
	
	private int index = 0;
	private AccessModifier current;
	private Tooltip tooltip = new Tooltip();
	
	public AccessModifierButton(Element logicElement, Restrictions restrictions){
		//initialization
		List<AccessModifier> allowedAccessMods = restrictions.allowedAccessMods();
		current = logicElement.getAccessMod();
		setTooltip(tooltip);
		tooltip.setText(current.getDescription());
		
		//events settings
		setOnAction(e -> {
			getStyleClass().remove(current.getStyleClassName());
			current = allowedAccessMods.get(++index % allowedAccessMods.size());
			getStyleClass().add(current.getStyleClassName());
			logicElement.setAccessMod(current);
			tooltip.setText(current.getDescription());
		});
		
		//layout settings
		getStyleClass().addAll(CSS_BASIC_STYLECLASS, current.getStyleClassName());		
		GUIUtils.setFixedSize(size, this);
		HBox.setMargin(this, margins);
	}
}
