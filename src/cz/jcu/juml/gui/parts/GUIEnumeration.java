package cz.jcu.juml.gui.parts;

import cz.jcu.juml.gui.others.AccessModifierButton;
import cz.jcu.juml.gui.others.GUIUtils;
import cz.jcu.juml.gui.others.InputTextField;
import cz.jcu.juml.gui.others.SettingsButton;
import cz.jcu.juml.logic.javaspec.parts.Method;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Label;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.Priority;
import javafx.scene.layout.Region;


public class GUIEnumeration extends HBox {

	public final AccessModifierButton btnAccessModifier;
	public final GUIParameterBox guiParameter;
	public final SettingsButton settingsButton;
        public final InputTextField txtIdentifier = new InputTextField(GUIUtils.promptTextIdentifier);

    public GUIEnumeration(Method enumeration, Pane guiParent) {
        //initialization
        btnAccessModifier = new AccessModifierButton(enumeration, enumeration.getParent().getEntityType().constructorRestrictions());
        guiParameter = new GUIParameterBox(enumeration,true);
        settingsButton = new SettingsButton(this)
                .addMoveToEditorButton(enumeration)
                .addMoveButtons(enumeration, this, guiParent)
                .addSeparator()
                .addRemoveButton(enumeration, this, guiParent)
                .addDebugButtons(enumeration);
        txtIdentifier.setText(enumeration.identifierProperty().getValue());
        txtIdentifier.textProperty().addListener((o,oldValue,newValue)->{
                txtIdentifier.textProperty().setValue(newValue.toUpperCase());
            });
        
        //bindings
        enumeration.identifierProperty().bind(txtIdentifier.textProperty());
        
        //button settings
        setOnMouseEntered(e -> {
            settingsButton.setVisible(true);
        });

        setOnMouseExited(e -> {
            settingsButton.setVisible(settingsButton.isFocused());
        });

        //label settings
        HBox.setMargin(txtIdentifier, new Insets(0, 2, 4, 7));

        //layout settings
        setAlignment(Pos.CENTER_LEFT);
        setPadding(new Insets(0, 0, 0, 3));
        Region region = new Region();
        HBox.setHgrow(region, Priority.ALWAYS);
        getChildren().addAll(
				btnAccessModifier,
				txtIdentifier,
				guiParameter,
				GUIUtils.regionFiller(),
				settingsButton);
    }
}
