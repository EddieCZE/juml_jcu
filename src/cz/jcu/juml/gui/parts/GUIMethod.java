package cz.jcu.juml.gui.parts;

import cz.jcu.juml.gui.others.AccessModifierButton;
import cz.jcu.juml.gui.others.GUIUtils;
import cz.jcu.juml.gui.others.InputTextField;
import cz.jcu.juml.gui.others.SettingsButton;
import cz.jcu.juml.logic.javaspec.parts.Method;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Label;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;

public class GUIMethod extends HBox {

	public final AccessModifierButton btnAccessModifier;
	public final InputTextField txtIdentifier = new InputTextField(GUIUtils.promptTextIdentifier);
	public final Label lblColonSeparator = new Label(":");	
	public final InputTextField txtReturnType = new InputTextField(GUIUtils.promptTextDataType);
	public final GUIParameterBox guiParameter;
	public final SettingsButton settingsButton;

	public GUIMethod(Method method, Pane guiParent) {
		//initialization
		btnAccessModifier = new AccessModifierButton(method, method.getParent().getEntityType().methodRestrictions());
		guiParameter = new GUIParameterBox(method,false);		
		settingsButton =  new SettingsButton(this)
			.addMoveToEditorButton(method)
			.addNonAccessModButton(method, method.getParent().getEntityType().methodRestrictions(), txtIdentifier)
			.addSeparator()
			.addMoveButtons(method, this, guiParent)
			.addSeparator()
			.addRemoveButton(method, this, guiParent)
			.addDebugButtons(method);

		txtIdentifier.setText(method.identifierProperty().getValue());
		txtReturnType.setText(method.typeProperty().getValue());

		//bindings
		method.identifierProperty().bind(txtIdentifier.textProperty());
		method.typeProperty().bind(txtReturnType.textProperty());

		//button settings
		setOnMouseEntered(e -> {
			settingsButton.setVisible(true);
		});

		setOnMouseExited(e -> {
			settingsButton.setVisible(settingsButton.isFocused());
		});

		//textfield settings
		GUIUtils.asDataTypeTextField(txtReturnType);

		//label settings
		lblColonSeparator.setMinWidth(Double.MIN_VALUE);

		//layout settings
		setAlignment(Pos.CENTER_LEFT);
		setPadding(new Insets(0, 0, 0, 3));

		getChildren().addAll(
				btnAccessModifier,
				txtIdentifier,
				guiParameter,
				lblColonSeparator,
				txtReturnType,
				GUIUtils.regionFiller(),
				settingsButton);
	}
}