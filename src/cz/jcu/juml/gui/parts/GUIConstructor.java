package cz.jcu.juml.gui.parts;

import cz.jcu.juml.gui.others.AccessModifierButton;
import cz.jcu.juml.gui.others.SettingsButton;
import cz.jcu.juml.logic.javaspec.parts.Method;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Label;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.Priority;
import javafx.scene.layout.Region;

public class GUIConstructor extends HBox {

	public final AccessModifierButton btnAccessModifier;
	public final Label lblIdentifier = new Label();
	public final GUIParameterBox guiParameter;
	public final SettingsButton settingsButton;

    public GUIConstructor(Method constructor, Pane guiParent) {
        //initialization
        btnAccessModifier = new AccessModifierButton(constructor, constructor.getParent().getEntityType().constructorRestrictions());
        guiParameter = new GUIParameterBox(constructor,false);
        settingsButton = new SettingsButton(this)
                .addMoveToEditorButton(constructor)
                .addMoveButtons(constructor, this, guiParent)
                .addSeparator()
                .addRemoveButton(constructor, this, guiParent)
                .addDebugButtons(constructor);
        //bindings
        lblIdentifier.textProperty().bind(constructor.getParent().identifierProperty());
        
        //button settings
        setOnMouseEntered(e -> {
            settingsButton.setVisible(true);
        });

        setOnMouseExited(e -> {
            settingsButton.setVisible(settingsButton.isFocused());
        });

        //label settings
        HBox.setMargin(lblIdentifier, new Insets(0, 2, 4, 7));

        //layout settings
        setAlignment(Pos.CENTER_LEFT);
        setPadding(new Insets(0, 0, 0, 3));
        Region region = new Region();
        HBox.setHgrow(region, Priority.ALWAYS);
        getChildren().addAll(btnAccessModifier, lblIdentifier, guiParameter, region, settingsButton);
    }
}
