
package cz.jcu.juml.gui.others;

public interface Stylizable {
	
	public String getStyleClassName();
}
