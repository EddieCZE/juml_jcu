package cz.jcu.juml.gui.others;

import java.util.Collection;
import java.util.Collections;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.scene.control.ContextMenu;
import javafx.scene.control.Menu;
import javafx.scene.control.MenuItem;
import javafx.scene.control.ScrollBar;
import javafx.scene.input.KeyCode;
import javafx.scene.input.MouseButton;
import javafx.scene.layout.AnchorPane;
import javafx.scene.paint.Color;
import org.fxmisc.richtext.CodeArea;
import org.fxmisc.richtext.LineNumberFactory;
import org.fxmisc.richtext.StyleSpans;
import org.fxmisc.richtext.StyleSpansBuilder;

import cz.jcu.juml.assets.AssetsProvider;
import cz.jcu.juml.logic.javaspec.parts.Method;

public class JavaCodeEditor extends CodeArea {

        private MenuItem ifCondition = new MenuItem("if");
        private MenuItem ifElseCondition = new MenuItem("if/else");
        private MenuItem whileCycle = new MenuItem("while");
        private MenuItem forCycle = new MenuItem("for");
        private MenuItem forEachCycle = new MenuItem("for each");
        private Menu menu = new Menu("Jazykov� struktury");
        private String currentText=this.getText();
        private int caretPosition=this.getCaretPosition();
        
        public ContextMenu languageStructures=new ContextMenu();
    
            
	private static final String[] KEYWORDS = new String[] {
		"abstract", "assert", "boolean", "break", "byte",
		"case", "catch", "class", "const",
		"continue", "char", "default", "do", "double", "else",
		"enum", "extends", "final", "finally", "float",
		"for", "goto", "if", "implements", "import", "int",
		"instanceof", "interface", "long", "native",
		"new", "package", "private", "protected", "public",
		"return", "static", "strictfp", "super", "short",
		"switch", "synchronized", "this", "throw", "throws",
		"transient", "try", "void", "volatile", "while"
	};

	private static final String[] PRIMITIVES = new String[] {
		"byte", "short", "int", "long",
		"float", "double",
		"char",
		"boolean"
	};
	
	
	private static final String KEYWORD_PATTERN = "\\b(" + String.join("|", KEYWORDS) + ")\\b";
	private static final String PAREN_PATTERN = "\\(|\\)";
	private static final String BRACE_PATTERN = "\\{|\\}";
	private static final String BRACKET_PATTERN = "\\[|\\]";
	private static final String SEMICOLON_PATTERN = "\\;";
	private static final String STRING_PATTERN = "\"([^\"\\\\]|\\\\.)*\"";
	private static final String COMMENT_PATTERN = "//[^\n]*" + "|" + "/\\*(.|\\R)*?\\*/";
	private static final String GENERICS_PATTERN = "\\<|\\>";
	private static final String REFERENCE_PATTERN = "[A-Z]+[a-zA-Z0-9]*";
	
	public static final ObjectProperty<Color> KEYWORD_COLOR = new SimpleObjectProperty<Color>(Color.BLACK);
	public static final ObjectProperty<Color> PRIMITIVE_COLOR = new SimpleObjectProperty<Color>(Color.FIREBRICK);
	public static final ObjectProperty<Color> REFERENCE_COLOR = new SimpleObjectProperty<Color>(Color.BLACK);
	public static final ObjectProperty<Color> STRING_COLOR = new SimpleObjectProperty<Color>(Color.BLUE);
	public static final ObjectProperty<Color> PAREN_COLOR = new SimpleObjectProperty<Color>(Color.BLACK);
	public static final ObjectProperty<Color> BRACE_COLOR = new SimpleObjectProperty<Color>(Color.BLACK);
	public static final ObjectProperty<Color> BRACKET_COLOR = new SimpleObjectProperty<Color>(Color.BLACK);
	public static final ObjectProperty<Color> SEMICOLON_COLOR = new SimpleObjectProperty<Color>(Color.BLACK);
	public static final ObjectProperty<Color> GENERICS_COLOR = new SimpleObjectProperty<Color>(Color.FUCHSIA);
	public static final ObjectProperty<Color> COMMENT_COLOR = new SimpleObjectProperty<Color>(Color.GREEN);

	
	
	private static final Pattern PATTERN = Pattern.compile(
			"(?<KEYWORD>" + KEYWORD_PATTERN + ")"
					+ "|(?<PAREN>" + PAREN_PATTERN + ")"
					+ "|(?<BRACE>" + BRACE_PATTERN + ")"
					+ "|(?<BRACKET>" + BRACKET_PATTERN + ")"
					+ "|(?<SEMICOLON>" + SEMICOLON_PATTERN + ")"
					+ "|(?<STRING>" + STRING_PATTERN + ")"
					+ "|(?<COMMENT>" + COMMENT_PATTERN + ")"
					+ "|(?<GENERICS>" + GENERICS_PATTERN + ")"
					+ "|(?<REFERENCE>" + REFERENCE_PATTERN + ")"
			);

	public static final String sampleCode = String.join("\n", new String[] {
			"package com.example;",
			"",
			"import java.util.*;",
			"",
			"public class Test {",
			" ",
			"    public static void main(String[] args) {"
	});
	//
        
        public ContextMenu initLanguageStuctures(){
            //languageStructures= new ContextMenu(ifCondition,ifElseCondition,whileCycle,forCycle,forEachCycle);
            //menu= new Menu("Jazykov� struktury", this, ifCondition,ifElseCondition,whileCycle,forCycle,forEachCycle);
            ifCondition.setOnAction(e->{
                this.insertText("if(/*podm�nka*/){\n\n}");
            });
            ifElseCondition.setOnAction(e->{
                this.insertText("if(podm�nka){\n\n}\nelse{\n\n}");
            });
            whileCycle.setOnAction(e->{
                this.insertText("while(podm�nka){\n\n}");
            });
            forCycle.setOnAction(e->{
                this.insertText("for(int i=0;i>0;i++){\n\n}");
            });
            forEachCycle.setOnAction(e->{
                this.insertText("for(Object o : Iterator<E>){\n\n}");
            });
            menu.getItems().addAll(ifCondition,ifElseCondition,whileCycle,forCycle,forEachCycle);
            languageStructures.getItems().add(menu);
            return languageStructures;
        }
        
        private void insertText(String text){
            /*String bodyText=GUIManager.currentMethod.bodyProperty().getValue();
            this.currentText= GUIManager.currentMethod.bodyProperty().getValue();
            System.out.println(this.getCaretPosition());
            StringBuilder str = new StringBuilder(getCurrentText());
            System.out.println(str.length());
            String newText = new StringBuilder(getCurrentText()).insert(this.getCaretPosition(), text).toString();
            this.replaceText(newText);
            GUIManager.currentMethod.bodyProperty().setValue(currentText);
            System.out.println(this.getCaretPosition());
            System.out.println(newText.length());*/
            
            //this.currentText=newText;
            
            String currText = GUIManager.currentMethod.bodyProperty().getValue();
            String newText = new StringBuilder(currText).insert(getCaretPosition(), text).toString();
            int pom = getCaretPosition();
            replaceText(newText);
            positionCaret(pom + text.length());
            GUIManager.currentMethod.bodyProperty().setValue(newText);
        }
       
        public void setCurrentText(String newText){
            this.currentText=newText;
        }
        
        public String getCurrentText(){
            return this.currentText;
        }
	
        public JavaCodeEditor(String code){
    	AnchorPane.setTopAnchor(this, 0.0);
		AnchorPane.setRightAnchor(this, 0.0);
		AnchorPane.setLeftAnchor(this, 0.0);
		AnchorPane.setBottomAnchor(this, 0.0);
		
		getStylesheets().add("cz/jcu/juml/" + AssetsProvider.pathToCSS("java-keywords.css"));
		
		textProperty().addListener((obs, oldText, newText) -> {
			setStyleSpans(0, computeHighlighting(newText));
		});
		replaceText(0, 0, code);
	}

	public static StyleSpans<Collection<String>> computeHighlighting(String text) {
		Matcher matcher = PATTERN.matcher(text);
		int lastKwEnd = 0;
		StyleSpansBuilder<Collection<String>> spansBuilder
		= new StyleSpansBuilder<>();
		while(matcher.find()) {
			String styleClass = matcher.group("PAREN") != null ? "paren" :
									matcher.group("BRACE") != null ? "brace" :
										matcher.group("BRACKET") != null ? "bracket" :
											matcher.group("SEMICOLON") != null ? "semicolon" :
												matcher.group("STRING") != null ? "string" :
													matcher.group("COMMENT") != null ? "comment" :
														matcher.group("GENERICS") != null ? "generics" : null;

			String word = text.substring(matcher.start(), matcher.end());
			
			if(matcher.group("KEYWORD") != null){
				styleClass = "keyword";
				for(String primitive : PRIMITIVES){
					if(word.equals(primitive)){
						styleClass = "primitive";
						break;
					}
				}
			}
			
			if(matcher.group("REFERENCE") != null)
				styleClass = "reference";
			
			spansBuilder.add(Collections.emptyList(), matcher.start() - lastKwEnd);
			spansBuilder.add(Collections.singleton(styleClass), matcher.end() - matcher.start());
			lastKwEnd = matcher.end();
		}
		spansBuilder.add(Collections.emptyList(), text.length() - lastKwEnd);
		return spansBuilder.create();
	}
}
