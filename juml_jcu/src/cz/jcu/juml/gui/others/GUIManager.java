package cz.jcu.juml.gui.others;

import cz.jcu.juml.gui.controllers.GUIController;
import cz.jcu.juml.gui.controllers.GUIEditorController;
import cz.jcu.juml.gui.parts.GUIEntity;
import cz.jcu.juml.logic.javaspec.parts.Entity;
import cz.jcu.juml.logic.javaspec.parts.Method;
import cz.jcu.juml.logic.javaspec.parts.Part;
import cz.jcu.juml.logic.others.EntityManager;
import javafx.collections.ObservableList;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.effect.GaussianBlur;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;

public class GUIManager {


	
	//private static final List<GUIEntity> guiEntities = new ArrayList<GUIEntity>();

	public static GUIController guiController;
    public static GUIEditorController guiEditorController;
	public static Stage mainStage;
	public static Stage pathsSettingsDialog;
	public static Stage syntaxHighlightDialog;
	public static Stage aboutDialog;
        public static Stage editorStage;
        public static Method currentMethod;
	
	public static void showDialog(Stage stage, Parent root){
		GUIManager.guiController.mainBorderPane.setEffect(new GaussianBlur(5.0));
		stage.setScene(new Scene(root));
		stage.show();
	}
	public static void registerGUIController(GUIController newGuiController){
		GUIManager.guiController = newGuiController;
	}
	
	public static void registerMainStage(Stage stage){
		GUIManager.mainStage = stage;
	}

	public static GUIEntity getGUIEntity(Entity entity){
		ObservableList<Node> entities = guiController.projectDesignerPane.getChildren();
		for(Node node : entities){
			if(node instanceof GUIEntity){
				GUIEntity guiEntity = (GUIEntity) node;
				if(guiEntity.logicEntity.equals(entity))
					return guiEntity;
			}
		}
		return null;
	}

	public static GUIEntity createGUIEntity(Entity logicEntity){
		GUIEntity guiEntity = new GUIEntity(logicEntity, guiController.projectDesignerPane);
		guiEntity.scaleXProperty().bind(guiController.sliderZoom.valueProperty());
		guiEntity.scaleYProperty().bind(guiController.sliderZoom.valueProperty());
				
		guiController.projectDesignerPane.getChildren().add(guiEntity);
		return guiEntity;
	}

	public static void add(Part logicPart, Pane guiParent){
	}

	public static void removeAllEntities(){
		guiController.projectDesignerPane.getChildren().clear();
		EntityManager.removeAllEntities();
	}
}