package cz.jcu.juml.gui.others;

import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;

public class AlertProvider {

	public static Alert errorAlert() {
		Alert alert = new Alert(AlertType.ERROR);
		alert.setTitle("V aplikaci nastala chyba");
		alert.setHeaderText("Look, an Error Dialog");
		alert.setContentText("Ooops, there was an error!");
		return alert;
	}
	
	public static Alert exceptionAlert() {
		Alert alert = new Alert(AlertType.ERROR);
		alert.setTitle("V aplikaci nastala chyba");
		alert.setHeaderText("Look, an Error Dialog");
		alert.setContentText("Ooops, there was an error!");
		return alert;
	}
	
	public static Alert confirmationAlert() {
		Alert alert = new Alert(AlertType.ERROR);
		alert.setTitle("V aplikaci nastala chyba");
		alert.setHeaderText("Look, an Error Dialog");
		alert.setContentText("Ooops, there was an error!");
		return alert;
	}
}
