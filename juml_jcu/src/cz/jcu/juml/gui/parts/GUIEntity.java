package cz.jcu.juml.gui.parts;

import java.io.File;

import cz.jcu.juml.gui.others.AccessModifierButton;
import cz.jcu.juml.gui.others.GUIUtils;
import cz.jcu.juml.gui.others.InputTextField;
import cz.jcu.juml.gui.others.JavaCodeEditor;
import cz.jcu.juml.gui.others.SettingsButton;
import cz.jcu.juml.logic.javaspec.EntityType;
import cz.jcu.juml.logic.javaspec.parts.Attribute;
import cz.jcu.juml.logic.javaspec.parts.Entity;
import cz.jcu.juml.logic.javaspec.parts.Enumeration;
import cz.jcu.juml.logic.javaspec.parts.Method;
import cz.jcu.juml.logic.others.EntityManager;
import cz.jcu.juml.logic.others.LogicUtils;
import javafx.animation.Animation;
import javafx.animation.FadeTransition;
import javafx.animation.Transition;
import javafx.beans.property.SimpleStringProperty;
import javafx.geometry.Insets;
import javafx.geometry.Orientation;
import javafx.geometry.Pos;
import javafx.scene.Cursor;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.Separator;
import javafx.scene.control.Tooltip;
import javafx.scene.effect.DropShadow;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.Priority;
import javafx.scene.layout.Region;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.util.Duration;

public class GUIEntity extends VBox {

	public final InputTextField txtEntityIdentifier = new InputTextField(GUIUtils.promptTextIdentifier);
	public final JavaCodeEditor javaCodeEditor = new JavaCodeEditor("");
	public final AccessModifierButton btnAccessModifier;
	public final Label lblEntityType;
	public final SettingsButton settingsButton;
	private double x;
	private double y;
	private boolean isExpanded;
        
        public final MembersGroup groupOfEnumeration = new MembersGroup("V��tov� typy", "P��dat v��et");
	public final MembersGroup groupOfAttributes = new MembersGroup("Atributy", "P�idat atribut");
	public final MembersGroup groupOfConstructors = new MembersGroup("Konstruktory", "P�idat konstruktor");
	public final MembersGroup groupOfMethods = new MembersGroup("Metody", "P�idat metody");
	public final HBox headerFullInfo;
	public Entity logicEntity;

	public  final DropShadow compilationFailedEffect = new DropShadow();
	public  final DropShadow compilationSucceedEffect = new DropShadow();

	public  final Animation animationCompFailed;	
	public  final Animation animationCompSucceed;

	
	public GUIEntity(Entity entity, Pane guiParent) {
                //compile effects
		final int depth = 70;

		compilationFailedEffect.setOffsetY(0f);
		compilationFailedEffect.setOffsetX(0f);
		compilationFailedEffect.setColor(Color.RED);
		compilationFailedEffect.setWidth(depth);
		compilationFailedEffect.setHeight(depth);

		compilationSucceedEffect.setOffsetY(0f);
		compilationSucceedEffect.setOffsetX(0f);
		compilationSucceedEffect.setColor(Color.FORESTGREEN);
		compilationSucceedEffect.setWidth(depth);
		compilationSucceedEffect.setHeight(depth);

		animationCompFailed = new Transition() {{setCycleDuration(Duration.millis(500));}
			protected void interpolate(double frac) {
				Color oldColor = compilationFailedEffect.getColor();
				Color newColor = new Color(oldColor.getRed(), oldColor.getGreen(), oldColor.getBlue(), frac);
				compilationFailedEffect.setColor(newColor);
			}
		};

		animationCompSucceed = new Transition() {
			{
				setCycleDuration(Duration.millis(250));
				setCycleCount(2);
				setAutoReverse(true);
			}

			protected void interpolate(double frac) {
				Color oldColor = compilationSucceedEffect.getColor();
				Color newColor = new Color(oldColor.getRed(), oldColor.getGreen(), oldColor.getBlue(), frac);
				compilationSucceedEffect.setColor(newColor);
			}
		};
		
		//initialization
		this.logicEntity = entity;
		btnAccessModifier = new AccessModifierButton(entity, entity.getEntityType().entityRestrictions());
		lblEntityType = new Label(entity.getEntityType().getDescription().toUpperCase());

		//textfield settings
		txtEntityIdentifier.setTooltip(new Tooltip("N�zev entity"));

		//bindings (order matters)
		txtEntityIdentifier.textProperty().setValue(entity.identifierProperty().getValue());
		entity.identifierProperty().bind(txtEntityIdentifier.textProperty());

		//layout settings
		Region region = new Region();
		VBox headerIdAndType = new VBox(lblEntityType, txtEntityIdentifier);
		headerFullInfo = new HBox(btnAccessModifier, headerIdAndType, region);	

		//VBox.setMargin(this, new Insets(10, 5, 5, 5));
		//HBox.setMargin(headerFullInfo, new Insets(10, 5, 10, 0));
		HBox.setMargin(btnAccessModifier, new Insets(0, 10, 0, 7));
		HBox.setHgrow(region, Priority.ALWAYS);


		btnAccessModifier.setAlignment(Pos.BOTTOM_LEFT);
		headerIdAndType.setAlignment(Pos.CENTER_LEFT);

		getStyleClass().add("entity");
		headerFullInfo.getStyleClass().add("entity-header");

		String headerColor;
		switch(entity.getEntityType()){
		case CLASS:
			headerColor = "entity-class";
			break;
		case ABSTRACT_CLASS:
			headerColor = "entity-abstract-class";
			break;
		case ENUMERATION:
			headerColor = "entity-enumeration";
			break;
		default: //INTERFACE
			headerColor = "entity-interface";
			break;
		}
		headerFullInfo.getStyleClass().add(headerColor);

		getChildren().add(headerFullInfo);

		//initialization 2
		settingsButton = new SettingsButton(headerFullInfo)
		.addNonAccessModButton(entity, entity.getEntityType().entityRestrictions(), txtEntityIdentifier)
		.addSeparator()
		.addShowClassButton(entity)
		.addSeparator()
		.addCompileButton(entity, this)
		.addCompileAndRunButton(entity, this)	
		.addSeparator()																
		.addRemoveButton(entity, this, guiParent)
		.addDebugButtons(entity);

		//button settings
		HBox.setMargin(settingsButton, new Insets(0, 0, 0, 10));
		headerFullInfo.getChildren().add(settingsButton);

                
                //adding group of enumeration
                groupOfEnumeration.btnAddMember.setOnAction(e->{
                        Method enumeration = new Method(entity,false,true);
                        EntityManager.add(enumeration);
                        GUIEnumeration guiEnum = new GUIEnumeration(enumeration, groupOfEnumeration.members);
                        groupOfEnumeration.members.getChildren().add(guiEnum);
                        guiEnum.toFront();
                });
                
		//adding group of attributes
		groupOfAttributes.btnAddMember.setOnAction(e -> {
			Attribute attribute = new Attribute(entity);
			EntityManager.add(attribute);
			GUIAttribute guiAttribute = new GUIAttribute(attribute, groupOfAttributes.members);
			groupOfAttributes.members.getChildren().add(guiAttribute);
			guiAttribute.toFront();
		});

		//adding group of constructors (if not interface)
		if (entity.getEntityType().constructorRestrictions() != null) {
			groupOfConstructors.btnAddMember.setOnAction(e -> {
				Method constructor = new Method(entity, true,false);
				EntityManager.add(constructor);
				GUIConstructor guiConstructor = new GUIConstructor(constructor, groupOfConstructors.members);
				groupOfConstructors.members.getChildren().add(guiConstructor);
				guiConstructor.toFront();
			});
		}

		groupOfMethods.btnAddMember.setOnAction(e -> {
			Method method = new Method(entity, false,false);
                        method.setBody(new SimpleStringProperty(LogicUtils.tab()+"return;"));
			EntityManager.add(method);
			GUIMethod guiMethod = new GUIMethod(method, groupOfMethods.members);
			groupOfMethods.members.getChildren().add(guiMethod);
			guiMethod.toFront();
		});	

		//events
		headerFullInfo.setOnMousePressed(e -> {
			x = getLayoutX() - e.getSceneX();
			y = getLayoutY() - e.getSceneY();
			setCursor(Cursor.MOVE);
			toFront();
		});

		headerFullInfo.setOnMouseReleased(e -> {
			setCursor(Cursor.HAND);
			settingsButton.setVisible(true);
		});

		headerFullInfo.setOnMouseDragged(e -> {
			if (e.getSceneX() + x < 1 | e.getSceneY() + y < 1) return;
			setLayoutX(e.getSceneX() + x);
			setLayoutY(e.getSceneY() + y);
			settingsButton.setVisible(false);
		});

		headerFullInfo.setOnMouseEntered(e -> {
			setCursor(Cursor.HAND);
			settingsButton.setVisible(true);
		});

		headerFullInfo.setOnMouseEntered(e -> {
			settingsButton.setVisible(true);			
		});

		headerFullInfo.setOnMouseExited(e -> {
			settingsButton.setVisible(settingsButton.isFocused());
		});

		headerFullInfo.getStyleClass().add("entity-header_expanded");
		setOnMouseClicked(e -> {
			requestFocus();
			if(e.getClickCount() == 2){
				setIsExpanded(!isExpanded);
			}
		});


		//relationship
		/*if(entity.getEntityType() != EntityType.ENUMERATION){
			setOnDragOver(e -> {
				Dragboard db = e.getDragboard();
				ClipboardContent content = new ClipboardContent();
				if (e.getGestureSource() != this && db.hasString()) {
					e.acceptTransferModes(TransferMode.MOVE);
					content.put(GUIUtils.DATAFORMAT_ENTITY, entity);
				}
			});
		}*/

		setOnDragOver(e -> {
			e.consume();
		});

		setIsExpanded(true);

		//transitions
		final FadeTransition fadeTransition = new FadeTransition(Duration.millis(300), this);
		fadeTransition.setFromValue(0f);
		fadeTransition.setToValue(1f);
		fadeTransition.play();

		//reading entity content
		for(Method enumeration : entity.getEnums()){
			GUIEnumeration guiEnum = new GUIEnumeration(enumeration, groupOfEnumeration.members);
			groupOfEnumeration.members.getChildren().add(guiEnum);
			guiEnum.toFront();
		}
                
                for(Attribute attribute : entity.getAttributes()){
			GUIAttribute guiAttribute = new GUIAttribute(attribute, groupOfAttributes.members);
			groupOfAttributes.members.getChildren().add(guiAttribute);
			guiAttribute.toFront();
		}

		for(Method constructor : entity.getConstructors()){
			GUIConstructor guiConstructor = new GUIConstructor(constructor, groupOfConstructors.members);
			groupOfConstructors.members.getChildren().add(guiConstructor);
			guiConstructor.toFront();
		}

		for(Method method : entity.getMethods()){
			GUIMethod guiMethod = new GUIMethod(method, groupOfMethods.members);
			groupOfMethods.members.getChildren().add(guiMethod);
			guiMethod.toFront();
		}
	}

	public boolean isExpanded(){
		return isExpanded;
	}


	public void setIsExpanded(boolean isExpanded){
		if(this.isExpanded == isExpanded) return;
		this.isExpanded = isExpanded;
		if(isExpanded){ //ne�pln� ov��en�, ale funguje
			addGroups();
			headerFullInfo.getStyleClass().remove("entity-header_shortened");
			headerFullInfo.getStyleClass().add("entity-header_expanded");
		} else {
			removeGroups();
			headerFullInfo.getStyleClass().remove("entity-header_expanded");
			headerFullInfo.getStyleClass().add("entity-header_shortened");
		}
	}

	private void addGroups(){
                if(logicEntity.getEntityType() == EntityType.ENUMERATION)
                        getChildren().add(groupOfEnumeration);
		getChildren().add(groupOfAttributes);
		if(logicEntity.getEntityType().constructorRestrictions() != null)
			getChildren().add(groupOfConstructors);
		getChildren().add(groupOfMethods);
	}

	private void removeGroups(){
                 if(logicEntity.getEntityType() == EntityType.ENUMERATION)
                        getChildren().remove(groupOfEnumeration);
		getChildren().remove(groupOfAttributes);
		if(logicEntity.getEntityType().constructorRestrictions() != null)
			getChildren().remove(groupOfConstructors);
		getChildren().remove(groupOfMethods);
	}

	public void animateCompFailedEffect(){
		setEffect(compilationFailedEffect);
		animationCompFailed.play();
	}
        
        public boolean animateCompEffectResult(File file){
		if(file==null){
                    animateCompFailedEffect();
                    return false;
                }
                else{
                    animateCompSucceedEffect();
                    return true;
                }
	}

	public void animateCompSucceedEffect(){
		setEffect(compilationSucceedEffect);
		animationCompSucceed.onFinishedProperty().set(ex -> {
			setEffect(null);
		});
		animationCompSucceed.play();
	}

	private class MembersGroup extends VBox {

		Button btnAddMember = new Button("+");
		HBox header = new HBox();
		VBox members = new VBox();

		MembersGroup(String labelText, String tooltipText) {
			//initialization
			Separator separatorBefore = new Separator(Orientation.HORIZONTAL);
			Label lblTitle = new Label(labelText);
			Separator separatorAfter = new Separator(Orientation.HORIZONTAL);

			//controls settings
			separatorBefore.setMinWidth(10.0);
			separatorBefore.setPrefWidth(10.0);
			btnAddMember.setTooltip(new Tooltip(tooltipText));

			//events
			btnAddMember.setOnMouseEntered(e -> {
				header.getStyleClass().add("element_adding");
			});

			btnAddMember.setOnMouseExited(e -> {
				header.getStyleClass().remove("element_adding");
			});

			header.setOnMouseEntered(e -> {
				header.getChildren().add(btnAddMember);
			});

			header.setOnMouseExited(e -> {
				header.getChildren().remove(btnAddMember);
			});

			//layout settings
			header.setAlignment(Pos.CENTER_LEFT);
			VBox.setMargin(header, new Insets(3, 5, 5, 0));
			HBox.setMargin(btnAddMember, new Insets(0, 0, 0, 3));
			HBox.setMargin(lblTitle, new Insets(0, 10, 0, 5));
			HBox.setMargin(separatorAfter, new Insets(0, 5, 0, 0));
			HBox.setHgrow(separatorAfter, Priority.ALWAYS);
			btnAddMember.getStyleClass().addAll("rounded_transparent_button");
			separatorAfter.setMaxWidth(Double.MAX_VALUE);
			header.getChildren().addAll(separatorBefore, lblTitle, separatorAfter);
			getChildren().addAll(header, members);
		}
	}
}
