package cz.jcu.juml.gui.controllers;

import cz.jcu.juml.gui.others.GUIManager;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.image.ImageView;

public class GUIAboutController {

    @FXML
    private ImageView imgLogo;

    @FXML
    private Label lblName;

    @FXML
    private Button btnOK;

    @FXML
    private Label lblAuthor;

    @FXML
    private TextArea lblInfo;

    @FXML
    private Label lblVersion;
    
    @FXML
    void initialize() {
        assert imgLogo != null : "fx:id=\"imgLogo\" was not injected: check your FXML file 'GUIAbout.fxml'.";
        assert lblName != null : "fx:id=\"lblName\" was not injected: check your FXML file 'GUIAbout.fxml'.";
        assert lblAuthor != null : "fx:id=\"lblAuthor\" was not injected: check your FXML file 'GUIAbout.fxml'.";
        assert lblInfo != null : "fx:id=\"lblInfo\" was not injected: check your FXML file 'GUIAbout.fxml'.";
        assert lblVersion != null : "fx:id=\"lblVersion\" was not injected: check your FXML file 'GUIAbout.fxml'.";
        
        btnOK.setOnAction(e->{
        	GUIManager.aboutDialog.close();
        	GUIManager.guiController.mainBorderPane.setEffect(null);
        });

    }
    
    
}