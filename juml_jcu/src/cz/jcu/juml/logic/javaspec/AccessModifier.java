package cz.jcu.juml.logic.javaspec;

import cz.jcu.juml.gui.others.Stylizable;
import cz.jcu.juml.logic.others.Codeable;
import cz.jcu.juml.logic.others.Describable;

public enum AccessModifier implements Codeable, Describable, Stylizable {

    PRIVATE("private", "private", "Soukrom�"),
    PACKAGE_PRIVATE("", "package_private", "P��telsk�"),
    PROTECTED("protected", "protected", "Chr�n�n�"),
    PUBLIC("public", "public", "Ve�ejn�");

    private static final String CSS_CLASSNAME_PREFIX = "access_modifier_";

    private final String code;
    private final String cssClassName;
    private final String description;

    private AccessModifier(String code, String cssClassName, String description) {
        this.code = code;
        this.cssClassName = cssClassName;
        this.description = description;
    }

    @Override
    public String toJavaCode() {
        return code;
    }

    @Override
    public String getDescription() {
        return description;
    }

    @Override
    public String getStyleClassName() {
        return CSS_CLASSNAME_PREFIX + cssClassName;
    }
}
