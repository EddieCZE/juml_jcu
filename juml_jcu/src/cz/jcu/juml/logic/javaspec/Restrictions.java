package cz.jcu.juml.logic.javaspec;

import java.util.List;

public class Restrictions
{
	private List<AccessModifier> allowedAccessMods;
	private List<NonAccessModifier> allowedNonAccessMods;
	
	public Restrictions(List<AccessModifier> allowedAccessMods, List<NonAccessModifier> allowedNonAccessMods){
		this.allowedAccessMods = allowedAccessMods;
		this.allowedNonAccessMods = allowedNonAccessMods;
	}
	
	public List<AccessModifier> allowedAccessMods(){
		return allowedAccessMods;
	}
	
	public List<NonAccessModifier> allowedNonAccesMods(){
		return allowedNonAccessMods;
		
	}
	
	public boolean isAllowed(AccessModifier accessMod){
		for(AccessModifier current : allowedAccessMods){
			if(current == accessMod)
				return true;
		}
		return false;
	}
	
	public boolean isAllowed(NonAccessModifier nonAccessMod){
		for(NonAccessModifier current : allowedNonAccessMods){
			if(current == nonAccessMod)
				return true;
		}
		return false;
	}
	
	@Override
	public String toString() {
		return "allowedAccessMods: " + allowedAccessMods + ", allowedNonAccessMods: " + allowedNonAccessMods;
	}
}
