/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package cz.jcu.juml.logic.javaspec.parts;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import cz.jcu.juml.logic.javaspec.AccessModifier;
import cz.jcu.juml.logic.javaspec.NonAccessModifier;
import cz.jcu.juml.logic.others.LogicUtils;
import javafx.beans.property.SimpleStringProperty;

/**
 *
 * @author Eddie
 */
public class Enumeration extends Member implements Serializable{
    private static final long serialVersionUID = 1L;
     List<Parameter> parameters;
    transient SimpleStringProperty body;
    transient SimpleStringProperty comment;

    public Enumeration(AccessModifier accessMod, List<NonAccessModifier> nonAccessMods, SimpleStringProperty type, SimpleStringProperty identifier, List<Parameter> parameters, SimpleStringProperty comment, SimpleStringProperty body, Entity parent) {
        super(accessMod, nonAccessMods, identifier, type, parent);
        this.parameters = parameters;
        this.body = body;
        this.comment = comment;
    }
    public Enumeration(Entity parent){
         this(AccessModifier.PACKAGE_PRIVATE, new ArrayList<NonAccessModifier>(), new SimpleStringProperty(""), new SimpleStringProperty(""), new ArrayList<Parameter>(), new SimpleStringProperty(""), new SimpleStringProperty(""), parent);
    }

    public List<Parameter> getParameters() {
        return parameters;
    }

    public SimpleStringProperty getBody() {
        return body;
    }

    public SimpleStringProperty getComment() {
        return comment;
    }

    public void setBody(SimpleStringProperty body) {
        this.body = body;
    }

    public void setComment(SimpleStringProperty comment) {
        this.comment = comment;
    }
    
    public String getHeaderOfEnum() {
        return LogicUtils.toJavaCode(
                getParent().accessMod,
                LogicUtils.listToJavaCode("", "", getParent().nonAccessMods, " ", ""),
                getParent().entityType,
                getParent().identifier.getValue(),
                "{");
    }

    @Override
    public String toJavaCode() {
        return LogicUtils.toJavaCode(
                LogicUtils.listToJavaCode("", "", nonAccessMods, " ", ""),
                identifier.getValue(),
                LogicUtils.listToJavaCode("(", "", parameters, ", ", ")"),
                "{\n",
                body.getValue(),
                "\n" + LogicUtils.tab() + "}\n"
        );
    }

    @Override
    public String toString() {
        return super.toString() + ", numOfParams: " + parameters.size() + ", body: " + body;
    }
}
