package cz.jcu.juml.logic.javaspec.parts;

import java.io.Serializable;

import javafx.beans.property.SimpleStringProperty;

public class Parameter extends Part implements Serializable {

	private static final long serialVersionUID = 1L;
	
	transient SimpleStringProperty type;
    transient SimpleStringProperty identifier;
    boolean isFinal;
    boolean isArray;
    Method parent;

    public Parameter(SimpleStringProperty type, SimpleStringProperty identifier, boolean isFinal, Method parent) {
        this.type = type;
        this.identifier = identifier;
        this.isFinal = isFinal;
        this.parent = parent;
    }
    
   /* public Parameter(SimpleStringProperty type, SimpleStringProperty identifier, boolean isFinal, Enumeration parent) {
        this.type = type;
        this.identifier = identifier;
        this.isFinal = isFinal;
        this.parent = parent;
    }
*/
    public SimpleStringProperty typeProperty() {
        return type;
    }

    public SimpleStringProperty identifierProperty() {
        return identifier;
    }

    public boolean isFinal() {
        return isFinal;
    }

    public boolean isArray() {
        return isFinal;
    }
    
    public Method getParent() {
        return parent;
    }

    public void setType(SimpleStringProperty type) {
        this.type = type;
    }

    public void setIdentifier(SimpleStringProperty identifier) {
        this.identifier = identifier;
    }

    public void setIsFinal(boolean isFinal) {
        this.isFinal = isFinal;
    }

    public void setParent(Method parent) {
        this.parent = parent;
    }

    @Override
    public String toJavaCode() {
        StringBuilder sb = new StringBuilder();
        if (isFinal()) {
            sb.append("final");
            sb.append(" ");
        }
        sb.append(type.getValue());
        sb.append(" ");
        sb.append(identifier.getValue());
        return sb.toString();
    }

    @Override
    public String toString() {
        return "type: " + type.getValue() + ", identifier: " + identifier.getValue() + ", isFinal: " + isFinal + ", parentDetected: " + (parent != null);
    }

    @Override
    public boolean equals(Object o) {
        if (o == null) {
            return false;
        }
        if (o == this) {
            return true;
        }
        if (!(o instanceof Parameter)) {
            return false;
        }
        Parameter obj = (Parameter) o;
        return super.equals(obj)
                && this.type.equals(obj.type)
                && this.identifier.equals(obj.identifier)
                && this.isFinal == obj.isFinal
                && this.parent.equals(obj.parent);
    }
}
