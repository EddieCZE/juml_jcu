package cz.jcu.juml.logic.javaspec.parts;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import cz.jcu.juml.logic.javaspec.AccessModifier;
import cz.jcu.juml.logic.javaspec.NonAccessModifier;
import cz.jcu.juml.logic.others.LogicUtils;
import javafx.beans.property.SimpleStringProperty;

public final class Attribute extends Member implements Serializable {

    private static final long serialVersionUID = 1L;

    transient SimpleStringProperty value;

    public Attribute(Entity parent) {
        this(AccessModifier.PACKAGE_PRIVATE, new ArrayList<NonAccessModifier>(), new SimpleStringProperty(""), new SimpleStringProperty(""), new SimpleStringProperty(""), parent);
    }

    public Attribute(AccessModifier accessMod, List<NonAccessModifier> nonAccessMods, SimpleStringProperty type, SimpleStringProperty identifier, SimpleStringProperty value, Entity parent) {
        super(accessMod, nonAccessMods, identifier, type, parent);
        this.value = value;
    }

    public SimpleStringProperty valueProperty() {
        return value;
    }

    public boolean isInitialized() {
        return !value.getValue().equals("");
    }

    public void setValue(SimpleStringProperty value) {
        this.value = value;
    }

    @Override
    public String toJavaCode() {
        StringBuilder sb = LogicUtils.toJavaCodeBuilder(
                accessMod,
                LogicUtils.listToJavaCode("", "", nonAccessMods, " ", ""),
                type.getValue(),
                identifier.getValue());
        if (isInitialized()) {
            sb.append(" = " + value.getValue());
        }
        sb.append(";");
        return sb.toString();
    }

    @Override
    public String toString() {
        return super.toString() + ", value: " + value.getValue();
    }

    @Override
    public boolean equals(Object o) {
        if (o == null) {
            return false;
        }
        if (o == this) {
            return true;
        }
        if (!(o instanceof Attribute)) {
            return false;
        }
        Attribute obj = (Attribute) o;
        return super.equals(obj)
                && this.value.equals(obj.value);
    }
}
