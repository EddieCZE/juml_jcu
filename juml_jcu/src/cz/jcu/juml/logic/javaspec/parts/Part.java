package cz.jcu.juml.logic.javaspec.parts;

import java.io.Serializable;

import cz.jcu.juml.logic.others.Codeable;

public abstract class Part implements Codeable, Serializable {
	
	private static final long serialVersionUID = 1L;
	
}
