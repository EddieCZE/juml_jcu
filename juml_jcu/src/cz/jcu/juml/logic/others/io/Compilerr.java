package cz.jcu.juml.logic.others.io;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.tools.JavaCompiler;
import javax.tools.ToolProvider;

import cz.jcu.juml.logic.javaspec.parts.Entity;
import cz.jcu.juml.logic.others.EntityManager;

public class Compilerr {

    public static File errorOutputFile;

    public static File compileJavaClass(File directory, String fileName, String content) throws CompilerNotFoundException, FileNotFoundException, ClassNotFoundException, IOException {
        File javaClass = IOUtils.createFile(directory, fileName, ".java");
        IOUtils.writeToFile(javaClass, content);

        errorOutputFile = new File(directory + "\\" + fileName + "_chyby.txt");
        FileOutputStream errorStream = new FileOutputStream(errorOutputFile);
        int compilationResult = getCompiler().run(null, null, errorStream, javaClass.getAbsolutePath());

        errorStream.close();
        javaClass.delete();
        if (compilationResult == 0) {
            errorOutputFile.delete();
            return new File(directory + "\\" + fileName + ".class");
        }
        return null;
    }

    public static boolean compileMultipleJavaClasses(File directory, String[] fileNames, String[] content) throws CompilerNotFoundException, FileNotFoundException, ClassNotFoundException, IOException {
        String[] classPaths = new String[fileNames.length];
        for (int i = 0; i < fileNames.length; i++) {
            File javaClass = IOUtils.createFile(directory, fileNames[i], ".java");
            IOUtils.writeToFile(javaClass, content[i]);
            classPaths[i] = javaClass.getAbsolutePath();
        }
        errorOutputFile = new File(directory + "\\chyby.txt");
        FileOutputStream errorStream = new FileOutputStream(errorOutputFile);
        int compilationResult = getCompiler().run(null, null, errorStream, classPaths);

        errorStream.close();
        return compilationResult == 0;
    }

    public static File[] compileAllEntities(File directory) throws IOException, ClassNotFoundException {
        List<File> listOfFiles = new ArrayList<File>();
        for (Entity entity : EntityManager.entities) {
            File file = Compilerr.compileJavaClass(directory, entity.identifierProperty().getValue(), entity.toJavaCode());
            if (file != null) {
                listOfFiles.add(file);
            }
        }
        File[] arrayOfFiles = new File[listOfFiles.size()];
        return listOfFiles.toArray(arrayOfFiles);
    }

    public static JavaCompiler getCompiler() throws CompilerNotFoundException {
        System.setProperty("java.home", IOUtils.compilerPath);
        JavaCompiler javaCompiler = ToolProvider.getSystemJavaCompiler();

        if (javaCompiler == null) {
            throw new CompilerNotFoundException();
        }

        return javaCompiler;
    }
}
