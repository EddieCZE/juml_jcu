package cz.jcu.juml.logic.others.io;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.jar.Attributes;
import java.util.jar.JarEntry;
import java.util.jar.JarOutputStream;
import java.util.jar.Manifest;

public class JarExporter {

	public static int BUFFER_SIZE = 10240;

	public static void createJarArchive(File archiveFile, File[] tobeJared, String mainClassName) throws IOException {

		byte buffer[] = new byte[BUFFER_SIZE];
		//open archive file
		FileOutputStream stream = new FileOutputStream(archiveFile);

		//set jar manifest data
		Manifest manifest = new Manifest();
		manifest.getMainAttributes().put(Attributes.Name.MANIFEST_VERSION, "1.0");
		manifest.getMainAttributes().put(Attributes.Name.MAIN_CLASS, mainClassName);
		
		//create jar output stream with manifest
		JarOutputStream out = new JarOutputStream(stream, manifest);
		
		//append files which should be included to jar
		for (int i = 0; i < tobeJared.length; i++) {
			if (tobeJared[i] == null || !tobeJared[i].exists()
					|| tobeJared[i].isDirectory())
				continue; //just in case...

			//add archive entry
			JarEntry jarAdd = new JarEntry(tobeJared[i].getName());
			jarAdd.setTime(tobeJared[i].lastModified());
			out.putNextEntry(jarAdd);

			//write file to archive
			FileInputStream in = new FileInputStream(tobeJared[i]);
			while (true) {
				int nRead = in.read(buffer, 0, buffer.length);
				if (nRead <= 0)
					break;
				out.write(buffer, 0, nRead);
			}
			in.close();
		}

		out.close();
		stream.close();
	}
}
