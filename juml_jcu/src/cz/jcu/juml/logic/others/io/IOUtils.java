package cz.jcu.juml.logic.others.io;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.ArrayList;
import java.util.List;

import cz.jcu.juml.logic.javaspec.parts.Entity;
import cz.jcu.juml.logic.others.EntityManager;

public class IOUtils {
	
	public static String workspacePath = "C:\\";
	public static String compilerPath = "C:\\Program Files\\Java\\jdk1.8.0_45";
	
	public static File[] entitiesAsJavaFiles(File directory) throws IOException{
		List<File> listOfFiles = new ArrayList<File>();
		for(Entity entity : EntityManager.entities){
			File file = createFile(directory, entity.identifierProperty().getValue(), ".java");
			listOfFiles.add(file);
		}
		File[] arrayOfFiles = new File[listOfFiles.size()];
		return listOfFiles.toArray(arrayOfFiles);
	}
	   
    public static File createFile(File directory, String fileName, String suffix) throws IOException{
    	File file = new File(directory + "\\" + fileName + suffix);
    	file.createNewFile();
    	return file;
    }
    
    public static void writeToFile(File file, String content) throws IOException{
    	 FileWriter fw = new FileWriter(file);
         BufferedWriter bw = new BufferedWriter(fw);
         bw.write(content);
         bw.close();
    }
    
    public static void invokeMainMethod(Entity entity) throws NoSuchMethodException, IllegalAccessException, IllegalArgumentException, InvocationTargetException, ClassNotFoundException, IOException {
        String filePath = workspacePath + "\\";
        File file = new File(filePath);
        URL url = file.toURI().toURL();
        URL[] urls = new URL[]{url};
        
        URLClassLoader classLoader = new URLClassLoader(urls);
        
        String packageName = "TestovaciProjekt";
        Class<?> clazz = classLoader.loadClass(packageName + "." + entity.identifierProperty().getValue());
        
        Method method = clazz.getMethod("main", String[].class);
        String[] params = null;
        method.invoke(null, (Object) params);
        
        classLoader.close();
    }
    
    public static File createDirectory(String absolutePath){
    	File directory = new File(absolutePath);
		if(!directory.exists())
			directory.mkdir();
		return directory;
    }
    
    public static StringBuilder readFile(File file) throws IOException {
        BufferedReader br = new BufferedReader(new FileReader(file));
        String line = null;
        StringBuilder stringBuilder = new StringBuilder();
        String ls = System.getProperty("line.separator");

        while ((line = br.readLine()) != null) {
            stringBuilder.append(line);
            stringBuilder.append(ls);
        }
        stringBuilder.append(ls);
        br.close();
        return stringBuilder;

    }
}
