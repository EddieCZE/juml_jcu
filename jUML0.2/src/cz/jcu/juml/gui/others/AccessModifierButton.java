package cz.jcu.juml.gui.others;
import java.util.List;
import cz.jcu.juml.logic.javaspec.AccessModifier;
import cz.jcu.juml.logic.javaspec.Restrictions;
import cz.jcu.juml.logic.javaspec.parts.Element;
import javafx.geometry.Insets;
import javafx.scene.control.Button;
import javafx.scene.control.Tooltip;
import javafx.scene.layout.HBox;

/**
 * Custom made button for access modifiers with own CSS styles
 * 
 * @author Adam Chovanec
 *
 */
public final class AccessModifierButton extends Button {

	private static final int size = 16;//20;
	private static final Insets margins = new Insets(0, 0, 0, 5);
	private static final String CSS_BASIC_STYLECLASS = "access_modifier";
	
	private int index = 0;
	private AccessModifier current;
	private Tooltip tooltip = new Tooltip();
	
	/**
	 * Constructor initialize graphical AccessModiefiers.
	 * Assigns logic parts and their restriction that are permitted to switch between different kind of access modifiers.
	 * Sets action that switch between access modifiers.
	 * Sets CSS style and position of button     
	 * 
	 * @param logicElement sets the logic parts
	 * @param restrictions set of rules for specific element
	 */
	public AccessModifierButton(Element logicElement, Restrictions restrictions){
		//initialization
		List<AccessModifier> allowedAccessMods = restrictions.allowedAccessMods();
		current = logicElement.getAccessMod();
		setTooltip(tooltip);
		tooltip.setText(current.getDescription());
		
		//events settings
		setOnAction(e -> {
			getStyleClass().remove(current.getStyleClassName());
			current = allowedAccessMods.get(++index % allowedAccessMods.size());
			getStyleClass().add(current.getStyleClassName());
			logicElement.setAccessMod(current);
			tooltip.setText(current.getDescription());
		});
		
		//layout settings
		getStyleClass().addAll(CSS_BASIC_STYLECLASS, current.getStyleClassName());		
		GUIUtils.setFixedSize(size, this);
		HBox.setMargin(this, margins);
	}
}
