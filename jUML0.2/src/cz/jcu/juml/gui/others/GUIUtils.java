package cz.jcu.juml.gui.others;

import cz.jcu.juml.assets.AssetsProvider;
import cz.jcu.juml.logic.javaspec.DataType;
import cz.jcu.juml.logic.javaspec.parts.Entity;
import cz.jcu.juml.logic.others.EntityManager;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.scene.control.ContextMenu;
import javafx.scene.control.MenuItem;
import javafx.scene.effect.GaussianBlur;
import javafx.scene.image.Image;
import javafx.scene.input.DataFormat;
import javafx.scene.input.Dragboard;
import javafx.scene.input.TransferMode;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.Region;
import javafx.stage.Modality;
import javafx.stage.Stage;

/**
 * This class is responsible for formatting the various graphical component parts 
 * @author Adam Chovanec
 *
 */
public final class GUIUtils {
	public static final DataFormat DATAFORMAT_ENTITY_TYPE = new DataFormat("cz.jcu.uml.logic.javaspec.EntityType");
	public static final DataFormat DATAFORMAT_ENTITY = new DataFormat("cz.jcu.uml.logic.javaspec.parts.Entity");
	public static final DataFormat DATAFORMAT_COMMENT = new DataFormat("cz.jcu.uml.logic.others.Comment");	
	public static final String promptTextIdentifier = "n�zev";
	public static final String promptTextDataType = "typ";
	public static final String promptTextValue = "hodnota";

	public static final double BASIC_FONT_SIZE = 12;

	public static final GaussianBlur GAUSSIAN_BLUR = new GaussianBlur(5.0);

	private GUIUtils() {}
	/**
	 * Sets fixed sized of the component to prevent scattering of graphical components
	 * @param fixedSize
	 * @param regions
	 */
	public static void setFixedSize(int fixedSize, Region... regions) {
		for (Region region : regions) {
			region.setMinSize(fixedSize, fixedSize);
			region.setPrefSize(fixedSize, fixedSize);
			region.setMaxSize(fixedSize, fixedSize);
		}
	}

	/**
	 * Real-time initialization of primitive datatypes or object datatypes from user.
	 * 
	 * Basic relationship of two entities.
	 * User is allowed to Drag&Drop other entities as Datatype
	 * Listener is present to check if user dragging name of the entity into Datatype of other entity
	 * Break this relationship via context menu if user right-click on created object datatype
	 * @param textfield
	 */
	public static void asDataTypeTextField(InputTextField textfield) {
		MenuItem item = new MenuItem("Odstranit vztah");
		ContextMenu cm = new ContextMenu();
		cm.getItems().add(item);

		ChangeListener<String> changeListener = new ChangeListener<String>(){
			@Override
			public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
				textfield.setText(newValue);
			}};

			textfield.setOnDragOver(e -> {
				if (e.getGestureSource() instanceof InputTextField
						&& e.getGestureSource() != textfield
						&& e.getDragboard().hasString()) 
				{
					e.acceptTransferModes(TransferMode.COPY_OR_MOVE);
				}
				e.consume();
			});

			textfield.setOnDragDropped(e -> {
				Dragboard db = e.getDragboard();
				Entity entity = EntityManager.getEntityIfExists(db.getString());
				if(entity != null){
					textfield.setEditable(false);
					entity.identifierProperty().addListener(changeListener);
					textfield.setText(db.getString());
					textfield.setContextMenu(cm);
					item.setOnAction(x -> {
						entity.identifierProperty().removeListener(changeListener);
						textfield.setText("");
						textfield.setEditable(true);
						textfield.setContextMenu(null);
					});
				}
				e.consume();
			});


			final String regex = "^[a-zA-Z]{1}([a-zA-Z0-9])*$";
			textfield.setRegex(regex);
			textfield.getStyleClass().add("text-field_bold");
			textfield.textProperty().addListener((observable, oldText, newText) -> {
				DataType newDataType = DataType.OBJECT;
				for (DataType current : DataType.values()) {
					if (newText.equals(current.toJavaCode())) {
						newDataType = current;
					}
				}			
				textfield.setStyle("-fx-text-fill: " + newDataType.getStringColor() + ";");
			});


	}
	/**
	 * Real-time initialization of value from user.
	 * 
	 * @param textfield
	 */
	
	public static void asValueTextField(InputTextField textfield) {
		textfield.textProperty().addListener((observable, oldText, newText) -> {
			if (newText.length() > 1) {
				String color;
				if (newText.charAt(0) == '"' && newText.charAt(newText.length() - 1) == '"') {
					color = "blue";
				} else {
					color = "black";
				}
				textfield.setStyle("-fx-text-fill: " + color + ";");
			}
		});
	}

	/**
	 * Creating space between GUI components
	 * @return region
	 */
	public static Region regionFiller(){
		Region region = new Region();
		HBox.setHgrow(region, Priority.ALWAYS);
		return region;
	}
	/**
	 * Switches between stages to show other dialog windows
	 * 
	 * @param title of dialog window
	 * @return focused stage
	 */
	public static Stage initCustomDialog(String title){
		Stage stage = new Stage();
		stage.setTitle(title);
        stage.getIcons().add(new Image(AssetsProvider.pathToImage("favicon.png")));
		stage.initModality(Modality.APPLICATION_MODAL);
		stage.setResizable(false);

		stage.setOnShowing(e -> {
			GUIManager.guiController.mainBorderPane.setEffect(GUIUtils.GAUSSIAN_BLUR);
		});
		stage.setOnCloseRequest(e -> {
			GUIManager.guiController.mainBorderPane.setEffect(null);
		});
		return stage;
	}
}
