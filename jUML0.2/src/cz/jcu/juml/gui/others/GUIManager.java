package cz.jcu.juml.gui.others;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import cz.jcu.juml.gui.controllers.GUIController;
import cz.jcu.juml.gui.controllers.GUIEditorController;
import cz.jcu.juml.gui.parts.GUIComment;
import cz.jcu.juml.gui.parts.GUIEntity;
import cz.jcu.juml.logic.javaspec.parts.Entity;
import cz.jcu.juml.logic.javaspec.parts.Method;
import cz.jcu.juml.logic.others.Comment;
import cz.jcu.juml.logic.others.EntityManager;
import cz.jcu.juml.logic.others.io.IOUtils;
import cz.jcu.juml.logic.others.io.Serializer;
import javafx.collections.ObservableList;
import javafx.scene.Node;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
/**
 * This class servers as provider of useful tools (static methods) for managing whole GUI environment
 * Also serves as a container for some classes to get easy access
 * 
 * 
 * @author Adam Chovanec
 *
 */
public class GUIManager {


	
	//private static final List<GUIEntity> guiEntities = new ArrayList<GUIEntity>();

	public static GUIController guiController;
    public static GUIEditorController guiEditorController;
	public static Stage mainStage;
	public static Stage pathsSettingsDialog;
	public static Stage syntaxHighlightDialog;
	public static Stage aboutDialog;
    public static Stage editorStage;
    public static Method currentMethod;
    public static final List<Comment> comments = new ArrayList<Comment>();
    
	/*
    public static void showDialog(Stage stage, Parent root){
		GUIManager.guiController.mainBorderPane.setEffect(new GaussianBlur(5.0));
		stage.setScene(new Scene(root));
		stage.show();
	}
    */
    
    /**
     * Method used for registering main GUI controller
     * @param newGuiController controllers responsible for initializing FXML files
     */
	public static void registerGUIController(GUIController newGuiController){
		GUIManager.guiController = newGuiController;
	}
	/**
     * Method used for registering editor GUI controller
     * @param newGuiController controllers responsible for initializing FXML files
     */
	public static void registerGUIEditorController(GUIEditorController newGuiController){
		GUIManager.guiEditorController = newGuiController;
	}
	/**
	 * Initialize main stage
	 * @param stage
	 */
	public static void registerMainStage(Stage stage){
		GUIManager.mainStage = stage;
	}
	/**
	 * Method used in serialization to check if the entity is created on project pane
	 * @param entity checks if the logic entity was created
	 * @return graphical representation of logical entity or non if not created
	 */
	public static GUIEntity getGUIEntity(Entity entity){
		ObservableList<Node> entities = guiController.projectDesignerPane.getChildren();
		for(Node node : entities){
			if(node instanceof GUIEntity){
				GUIEntity guiEntity = (GUIEntity) node;
				if(guiEntity.logicEntity.equals(entity))
					return guiEntity;
			}
		}
		return null;
	}
	
	/**
	 * Method used in serialization to check if the comment is created on project pane
	 * @param comment checks if the logic comment was created
	 * @return graphical representation of logical comment or non if not created
	 */
	public static GUIComment getGUIComment(Comment comment){
		ObservableList<Node> comments = guiController.projectDesignerPane.getChildren();
		for(Node node : comments){
			if(node instanceof GUIComment){
				GUIComment guiComment = (GUIComment) node;
				if(guiComment.getComment().equals(comment))
					return guiComment;
			}
		}
		return null;
		
	}
	/**
	 * Creates GUI entity from logic entity.
	 * Used in deserialization to create exact graphical and logical entity when saved
	 * Add GUI Entity to project pane with all settings (size, location, zoom, etc.) 
	 * @param logicEntity to create
	 * @return graphical representation of entity 
	 */
	public static GUIEntity createGUIEntity(Entity logicEntity){
		GUIEntity guiEntity = new GUIEntity(logicEntity, guiController.projectDesignerPane);
		guiEntity.scaleXProperty().bind(guiController.sliderZoom.valueProperty());
		guiEntity.scaleYProperty().bind(guiController.sliderZoom.valueProperty());
				
		guiController.projectDesignerPane.getChildren().add(guiEntity);
		return guiEntity;
	}
	
	/**
	 * Creates GUI comment from logic comment.
	 * Used in deserialization to create exact graphical and logical comment when saved
	 * Add GUI Comment to project pane with all settings (size, location, zoom, etc.) 
	 * @param logicComment to create
	 * @return graphical representation of comment 
	 */
	public static GUIComment createGUIComment(Comment logicComment){
		GUIComment guiComment = new GUIComment(logicComment, guiController.projectDesignerPane);
		guiComment.scaleXProperty().bind(guiController.sliderZoom.valueProperty());
		guiComment.scaleYProperty().bind(guiController.sliderZoom.valueProperty());
				
		guiController.projectDesignerPane.getChildren().add(guiComment);
		return guiComment;
		
	}
	/*
	public static void add(Part logicPart, Pane guiParent){
	}
	 */
	/**
	 * Method that removes all entities on project pane.
	 * Reset entity counter
	 * Used in newProject function
	 */
	public static void removeAllEntities(){
		guiController.projectDesignerPane.getChildren().clear();
		EntityManager.removeAllEntities();
	}
	/**
	 * Managing save project function to create and show FileChooser dialog window
	 * Actual save of project to the file with custom suffix .juml using Serializer class
	 */
	public static void saveProject(){
        try {
            FileChooser fileChooser = new FileChooser();
            fileChooser.getExtensionFilters().add(new FileChooser.ExtensionFilter("soubor jUML (*.juml)", "*.juml"));
            fileChooser.setInitialDirectory(new File(IOUtils.workspacePath));
            GUIManager.guiController.mainBorderPane.setEffect(GUIUtils.GAUSSIAN_BLUR);
            File file = fileChooser.showSaveDialog(GUIManager.mainStage);
            if (file != null) {
                Serializer.serializeProject(file);
            }
            GUIManager.guiController.mainBorderPane.setEffect(null);
        } catch (Exception ex) {
        	AlertProvider.errorAlert("P�i ukl�d�n� projektu se vyskytla neo�ek�van� chba", "Chyba p�i ukl�d�n� projektu");
        }
    }
	
	/**
	 * Managing load project function to create and show FileChooser dialog window
	 * Actual load of project from the file with custom suffix .juml using Serializer class
	 */
	public static void loadProject(){
        try {
            FileChooser fileChooser = new FileChooser();
            fileChooser.getExtensionFilters().add(new FileChooser.ExtensionFilter("soubor jUML (*.juml)", "*.juml"));
            fileChooser.setInitialDirectory(new File(IOUtils.workspacePath));
            GUIManager.guiController.mainBorderPane.setEffect(GUIUtils.GAUSSIAN_BLUR);
            File file = fileChooser.showOpenDialog(GUIManager.mainStage);
            GUIManager.guiController.mainBorderPane.setEffect(null);
            if (file != null) {
                GUIManager.removeAllEntities();
                Serializer.deserializeProject(file);
            }
        } catch (Exception e1) {
            AlertProvider.errorAlert("P�i na��t�n� projektu se vyskytla neo�ek�van� chba", "Chyba p�i na��t�n� projektu");
        }
    }
}