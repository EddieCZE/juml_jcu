package cz.jcu.juml.gui.others;

import java.io.PrintWriter;
import java.io.StringWriter;

import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.ButtonBar.ButtonData;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Priority;
/**
 * This class serves as dialog window provider to variety GUI components.
 * Creates some generic dialogs such as error, confirm or information windows
 * Customize some dialogs for specific functions
 * 
 * @author Adam Chovanec
 *
 */
public class AlertProvider {

	/**
	 * Static error dialog with custom header and its content
	 * @param content what is the meaning of the dialog
	 * @param header summarize the reason of popping out
	 * @return initialized dialog
	 */
	public static Alert errorAlert(String content, String header) {
		Alert alert = new Alert(AlertType.ERROR);
		alert.setTitle("Nastala chyba");
		alert.setHeaderText(header);
		alert.setContentText(content);
		return alert;
	}
	/**
	 * Custom confirmation dialog provided in onClose methods
	 * Used in stage in setOnCloseRequest method to prevent unintentional close of application
	 * Provides own sets of buttons
	 * @param content what is the meaning of the dialog
	 * @param header summarize the reason of popping out
	 * @return initialized dialog with custom button
	 */
	public static Alert exitWarningConfirm(String content, String header) {
		Alert alert = new Alert(AlertType.CONFIRMATION);
		alert.setTitle("Upozorn�n�");
		alert.setHeaderText(header);
		alert.setContentText(content);
		ButtonType ok = new ButtonType("OK", ButtonData.OK_DONE);
		ButtonType cancel = new ButtonType("Cancel", ButtonData.CANCEL_CLOSE);
		ButtonType saveProject = new ButtonType("Ulo�it");
		alert.getButtonTypes().setAll(ok,cancel,saveProject);
		return alert;
	}
	
	/**
	 * Static test error dialog
	 * @param content what is the meaning of the dialog
	 * @param header summarize the reason of popping out
	 * @return initialized dialog
	 */
	public static Alert exceptionAlert() {
		Alert alert = new Alert(AlertType.ERROR);
		alert.setTitle("V aplikaci nastala chyba");
		alert.setHeaderText("Look, an Error Dialog");
		alert.setContentText("Ooops, there was an error!");
		return alert;
	}
	/**
	 * Error dialogue showed by source code generation method 
	 * @param content what is the meaning of the dialog
	 * @return initialized dialog
	 */
	public static Alert GeneretionFailed(String content) {
		Alert alert = new Alert(AlertType.ERROR);
		alert.setTitle("Chyba");
		alert.setHeaderText("Generov�n� zdrojov�ho k�du");
		alert.setContentText(content);
		return alert;
	}
	/**
	 * Success information dialogue during source code generation method  
	 * @param content what is the meaning of the dialog
	 * @return initialized dialog
	 */
	public static Alert GeneretionInformationAlert(){
		Alert alert = new Alert(AlertType.INFORMATION);
		alert.setTitle("Informace");
		alert.setHeaderText("Generov�n� zdrojov�ho k�du");
		alert.setContentText("Akce prob�hla �sp�n�");
		return alert;
		
	}
	/**
	 * System dialogue which catches any errors occurred during program execution
	 * Helps user or programmer to quickly find out the problem
	 * 
	 * @param ex catching exception occurred during execution
	 * @return dialog presented with errors
	 */
	public static Alert exceptionAlert(Exception ex) {
		Alert alert = new Alert(AlertType.ERROR);
		alert.setTitle("Nastala z�va�n� chyba");
		alert.setHeaderText(ex.getMessage());
		
		// Create expandable Exception.
		StringWriter sw = new StringWriter();
		PrintWriter pw = new PrintWriter(sw);
		ex.printStackTrace(pw);
		String exceptionText = sw.toString();

		Label label = new Label("Detailn� hl�en� o vznikl�m probl�mu:");

		TextArea textArea = new TextArea(exceptionText);
		textArea.setEditable(false);
		textArea.setWrapText(true);

		textArea.setMaxWidth(Double.MAX_VALUE);
		textArea.setMaxHeight(Double.MAX_VALUE);
		GridPane.setVgrow(textArea, Priority.ALWAYS);
		GridPane.setHgrow(textArea, Priority.ALWAYS);

		GridPane expContent = new GridPane();
		expContent.setMaxWidth(Double.MAX_VALUE);
		expContent.add(label, 0, 0);
		expContent.add(textArea, 0, 1);

		// Set expandable Exception into the dialog pane.
		alert.getDialogPane().setContent(expContent);

		return alert;
	}
	
	
}
