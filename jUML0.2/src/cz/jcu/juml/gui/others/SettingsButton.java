package cz.jcu.juml.gui.others;

import java.io.File;
import java.io.IOException;

import cz.jcu.juml.gui.controllers.GUIEditorController;
import cz.jcu.juml.gui.parts.GUIEntity;
import cz.jcu.juml.logic.javaspec.NonAccessModifier;
import cz.jcu.juml.logic.javaspec.Restrictions;
import cz.jcu.juml.logic.javaspec.parts.Attribute;
import cz.jcu.juml.logic.javaspec.parts.Element;
import cz.jcu.juml.logic.javaspec.parts.Entity;
import cz.jcu.juml.logic.javaspec.parts.Method;
import cz.jcu.juml.logic.javaspec.parts.Part;
import cz.jcu.juml.logic.others.EntityManager;
import cz.jcu.juml.logic.others.io.Compilerr;
import cz.jcu.juml.logic.others.io.IOUtils;
import javafx.scene.control.Button;
import javafx.scene.control.CheckMenuItem;
import javafx.scene.control.ContextMenu;
import javafx.scene.control.MenuItem;
import javafx.scene.control.SeparatorMenuItem;
import javafx.scene.control.Tooltip;
import javafx.scene.layout.Pane;
/**
 * For the purpose of custom settings it was developed specific settings button to provide various function to elements
 * To prevent windows pop outs it was created simple context menu that navigates user to several options 
 * 
 * 
 * @author Adam Chovanec
 *
 */
public class SettingsButton extends Button {

    private ContextMenu contextMenu = new ContextMenu();
    /**
     * Basic part of initialization sets properties and listeners of custom button and context menu 
     * @param guiParent
     */
    public SettingsButton(Pane guiParent) {
        super(">");
        getStyleClass().add("rounded_transparent_button");
        setTooltip(new Tooltip("Mo�nosti"));
        setVisible(false);
        contextMenu.showingProperty().addListener((observable, isHiding, isShowing) -> {
            if (isShowing) {
                guiParent.getStyleClass().add("element_editing");
            } else {
                guiParent.getStyleClass().remove("element_editing");
                setVisible(false);
            }
        });

        setOnMouseClicked(e -> {
            contextMenu.show(guiParent, e.getScreenX(), e.getScreenY());
        });

        focusedProperty().addListener((observable, lostFocus, hasFocus) -> {
            if (lostFocus) {
                contextMenu.hide();
            }
        });
    }
    /**
     * Settings button that enables user to move to the code editor
     * 
     * @param method edited method that is referred to the code editor
     * @return SettingsButton with moveToEditor function
     */
    public SettingsButton addMoveToEditorButton(Method method) {
        MenuItem menuItemToEditor = new MenuItem("P�esunout se do editoru");
        menuItemToEditor.setOnAction(e -> {
            GUIManager.guiController.javaCodeEditorPane.setVisible(true);
            GUIManager.currentMethod=method;
            GUIManager.guiController.javaCodeEditor1.replaceText(method.getHeader());
            GUIManager.guiController.javaCodeEditor2.replaceText(method.bodyProperty().getValue());
            //nabindovat na kl�vesu ulo�en� body v javaCodeEditoru
        });
        contextMenu.getItems().add(menuItemToEditor);
        return this;
    }
    /**
     * Settings button that enables user to inspect whole class in code editor
     * It's read-only function used as reference from log window
     * @param entity to inspect
     * @return SettingsButton with class inspect function
     */
    public SettingsButton addShowClassButton(Entity entity){
        MenuItem menuShowClass = new MenuItem("Zobrazit celou t��du");
        menuShowClass.setOnAction(e ->{
            GUIManager.editorStage.setTitle(entity.identifierProperty().getValue());
            GUIEditorController.javaCodeEditor.replaceText("");
            GUIEditorController.javaCodeEditor.replaceText(entity.toJavaCode());
            GUIManager.editorStage.show();
        });
        
        contextMenu.getItems().add(menuShowClass);
        return this;
    }

    //dirty method, more gui components created then really needed; calling unnesesary methods
    /**
     * Options that make elements non-accessible as abstract,static or final
     * Interpreted in various ways
     * static elements will be underlined
     * final elements will be bold
     * abstract class doesn't have these options because is already determinate
     * @param element uses non-access modifiers
     * @param restrictions related to the element that is able to use or not non-access modifiers 
     * @param textfield applies textfield's effect
     * @return
     */
    public SettingsButton addNonAccessModButton(Element element, Restrictions restrictions, InputTextField textfield) {
        char langfix = element instanceof Attribute ? '�' : '�';
        CheckMenuItem menuItemIsAbstract = new CheckMenuItem("Abstraktn�");
        CheckMenuItem menuItemIsStatic = new CheckMenuItem("Statick" + langfix);
        CheckMenuItem menuItemIsFinal = new CheckMenuItem("Kone�n" + langfix);

        boolean isPredetermined = !restrictions.isAllowed(NonAccessModifier.NONE);

        if (restrictions.isAllowed(NonAccessModifier.ABSTRACT)) {
            menuItemIsAbstract.setSelected(isPredetermined);
            menuItemIsAbstract.setDisable(isPredetermined);
            menuItemIsAbstract.selectedProperty().addListener((observable, wasSelected, isSelected) -> {
                if (isSelected) {
                    element.getNonAccessMods().add(NonAccessModifier.ABSTRACT);
                } else {
                    element.getNonAccessMods().remove(NonAccessModifier.ABSTRACT);
                }
                menuItemIsStatic.setDisable(isSelected);
                menuItemIsFinal.setDisable(isSelected);
                textfield.setStyle("-fx-font-style: " + (isSelected ? "oblique" : "normal") + ";");
            });
            contextMenu.getItems().add(menuItemIsAbstract);
        }
        if (restrictions.isAllowed(NonAccessModifier.STATIC)) {
            menuItemIsStatic.setSelected(isPredetermined);
            menuItemIsStatic.setDisable(isPredetermined);
            menuItemIsStatic.selectedProperty().addListener((observable, wasSelected, isSelected) -> {
                if (isSelected) {
                    element.getNonAccessMods().add(NonAccessModifier.STATIC);
                    textfield.getStyleClass().add("text-field_underlined");
                } else {
                    element.getNonAccessMods().remove(NonAccessModifier.STATIC);
                    textfield.getStyleClass().remove("text-field_underlined");
                }
                if (!menuItemIsFinal.isSelected()) {
                    menuItemIsAbstract.setDisable(isSelected);
                }
            });
            contextMenu.getItems().add(menuItemIsStatic);
        }
        if (restrictions.isAllowed(NonAccessModifier.FINAL)) {
            menuItemIsFinal.setSelected(isPredetermined);
            menuItemIsFinal.setDisable(isPredetermined);
            menuItemIsFinal.selectedProperty().addListener((observable, wasSelected, isSelected) -> {
                if (isSelected) {
                    element.getNonAccessMods().add(NonAccessModifier.FINAL);
                } else {
                    element.getNonAccessMods().remove(NonAccessModifier.FINAL);
                }
                if (!menuItemIsStatic.isSelected()) {
                    menuItemIsAbstract.setDisable(isSelected);
                }
                textfield.setStyle("-fx-font-weight: " + (isSelected ? "bold" : "normal") + ";");
            });
            contextMenu.getItems().add(menuItemIsFinal);
        }
        return this;
    }

    public SettingsButton addRemoveButton(Part logicPart, Pane guiElement, Pane guiParent) {
        MenuItem menuItemRemove = new MenuItem("Odstranit");
        menuItemRemove.setOnAction(e -> {
            guiParent.getChildren().remove(guiElement);
            EntityManager.remove(logicPart);
        });

        contextMenu.getItems().add(menuItemRemove);
        return this;
    }

    public SettingsButton addMoveButtons(Part logicPart, Pane guiElement, Pane guiParent) {
        MenuItem menuItemMoveTop = new MenuItem("P�esunout na za��tek");
        MenuItem menuItemMoveUp = new MenuItem("Posunout vp�ed");
        MenuItem menuItemMoveDown = new MenuItem("Posunout vzad");
        MenuItem menuItemMoveBottom = new MenuItem("P�esunout na konec");

        menuItemMoveTop.setOnAction(e -> {
            guiParent.getChildren().remove(guiElement);
            guiParent.getChildren().add(0, guiElement);
            EntityManager.remove(logicPart);
            EntityManager.add(0, logicPart);
        });

        menuItemMoveBottom.setOnAction(e -> {
            guiParent.getChildren().remove(guiElement);
            guiParent.getChildren().add(guiElement);
            EntityManager.remove(logicPart);
            EntityManager.add(logicPart);
        });

        menuItemMoveUp.setOnAction(e -> {
            int index = guiParent.getChildren().indexOf(guiElement) - 1;
            guiParent.getChildren().remove(guiElement);
            EntityManager.remove(logicPart);
            if (index > 0) {
                guiParent.getChildren().add(index, guiElement);
                EntityManager.add(index, logicPart);
            } else {
                guiParent.getChildren().add(0, guiElement);
                EntityManager.add(0, logicPart);
            }
        });

        menuItemMoveDown.setOnAction(e -> {
            int index = guiParent.getChildren().indexOf(guiElement) + 1;
            guiParent.getChildren().remove(guiElement);
            EntityManager.remove(logicPart);
            if (index <= guiParent.getChildren().size()) {
                guiParent.getChildren().add(index, guiElement);
                EntityManager.add(index, logicPart);
            } else {
                guiParent.getChildren().add(guiElement);
                EntityManager.add(logicPart);
            }
        });

        contextMenu.getItems().addAll(menuItemMoveTop, menuItemMoveUp, menuItemMoveDown, menuItemMoveBottom);
        return this;
    }

    /**
     * Registering custom menu item to the context menu
     * @param customMenuItem
     * @return SettingsButton with custom menu item 
     */
    public SettingsButton addCustomButton(MenuItem customMenuItem) {
        contextMenu.getItems().addAll(customMenuItem);
        return this;
    }

    /**
     * Adds separator between section of context menu
     * @return SettingsButton's se�arator
     */
    public SettingsButton addSeparator() {
        if (contextMenu.getItems().size() != 0) {
            contextMenu.getItems().add(new SeparatorMenuItem());
        }
        return this;
    }

    /**
     * Provides user some info tools like print to console
     * @param logicPart wants to inspect
     * @return SettingsButton with console commands 
     */
    public SettingsButton addDebugButtons(Part logicPart) {
        MenuItem menuToString = new MenuItem("print toString");
        MenuItem menuToJavaCode = new MenuItem("print toJavaCode");
        this.addSeparator();
        menuToString.setOnAction(e -> {
            System.out.println(logicPart.toString());
            GUIManager.guiController.txtErrors.setText(logicPart.toString());
        });
        menuToJavaCode.setOnAction(e -> {
            System.out.println(logicPart.toJavaCode());
            GUIManager.guiController.txtErrors.setText(logicPart.toJavaCode());
        });
        contextMenu.getItems().addAll(menuToString, menuToJavaCode);
        return this;
    }

    /**
     * Compilation option that runs javaCompiler of one specific class
     * @param logicEntity to compile
     * @param guiEntity creates graphical effect according to success of a compilation 
     * @return SettingButton that allows user to compile specific entity 
     */
    public SettingsButton addCompileButton(Entity logicEntity, GUIEntity guiEntity) {
        MenuItem menuCompile = new MenuItem("Zkompilovat");
        GUIManager.guiController.txtErrors.setText("");
        menuCompile.setOnAction(e -> {
            File compiledClass = null;
            StringBuilder errors = new StringBuilder("");
            try {
                File directory = IOUtils.createDirectory(IOUtils.workspacePath + "\\zkompilovane_entity");

                String className = logicEntity.identifierProperty().getValue();
                compiledClass = Compilerr.compileJavaClass(directory, className, logicEntity.toJavaCode());
                if (compiledClass == null) {
                    errors.append(IOUtils.readFile(Compilerr.errorOutputFile));
                    guiEntity.animateCompFailedEffect();
                } else {
                    guiEntity.animateCompSucceedEffect();
                }

            } catch (ClassNotFoundException ex) {
            	GUIManager.guiController.txtErrors.setText("Nenalezena t��da");//dialogs
            } catch (IOException ex) {
            	GUIManager.guiController.txtErrors.setText("I/O probl�m");//dialogs
            }

            if (!errors.toString().equals("")) {
                GUIManager.guiController.txtErrors.setText(errors.toString());
            } else {
                GUIManager.guiController.txtErrors.setText("Kompilace prob�hla �sp�n�");
            }
        });

        contextMenu.getItems().add(menuCompile);
        return this;
    }

    /**
     * Enhanced compilation option method that enable to compile and run entity
     * This function is not yet implemented!
     * 
     * @param logicEntity
     * @param guiEntity
     * @return
     */
    public SettingsButton addCompileAndRunButton(Entity logicEntity, GUIEntity guiEntity) {
        //if logicEntity.vsechnyMetody.maMainMetodu?
        MenuItem menuInvokeMain = new MenuItem("Zkompilovat a spustit");
        menuInvokeMain.setOnAction(e -> {
            try {
                IOUtils.invokeMainMethod(logicEntity);
            } catch (Exception ex) {
            	AlertProvider.errorAlert("Nen� p��tomna Main metoda", "Projekt nelze spustit").show();;
            }
        });
        contextMenu.getItems().add(menuInvokeMain);
        return this;
    }
}
