
package cz.jcu.juml.gui.others;
/**
 * Provides style names of classes to CSS file
 * @author Eddie
 *
 */
public interface Stylizable {
	
	public String getStyleClassName();
}
