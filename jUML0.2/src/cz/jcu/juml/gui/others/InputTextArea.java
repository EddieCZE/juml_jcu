package cz.jcu.juml.gui.others;

import com.sun.javafx.tk.Toolkit;

import javafx.scene.control.TextArea;

@SuppressWarnings("restriction")
/**
 * Custom TextArea for GUIComment:
 *
 * @author Adam Chovanec
 *
 */
public class InputTextArea extends TextArea {

	private static final int TEXTFIELD_PREFIX_WIDTH = 15; //25
	/**
	 * Managing size and properties of text area
	 * 
	 * @param promptText replacement text
	 */
	public InputTextArea(String promptText) {
			
		setPromptText(promptText);
		computeWidthToTextLength(promptText);
		setEventHandlers();
		
		setMinSize(100, 50);
		setMaxSize(300, 300);
		setWrapText(true);
	}

	private void setEventHandlers(){		
		textProperty().addListener((observable, oldText, newText) -> {
			setText(newText);
			computeWidthToTextLength(newText);
		});

		focusedProperty().addListener((observable, lostFocus, hasFocus) -> {
				if(getText().length() == 0 && lostFocus)
					computeWidthToTextLength(getPromptText());
		});

	}

	private void computeWidthToTextLength(String text){	
		float width = TEXTFIELD_PREFIX_WIDTH + Toolkit.getToolkit().getFontLoader().computeStringWidth(text, getFont());
		setPrefWidth(width);
	}
}
