package cz.jcu.juml.gui.parts;

import cz.jcu.juml.gui.others.AccessModifierButton;
import cz.jcu.juml.gui.others.GUIUtils;
import cz.jcu.juml.gui.others.InputTextField;
import cz.jcu.juml.gui.others.SettingsButton;
import cz.jcu.juml.logic.javaspec.parts.Attribute;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Label;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;

/**
 * Creates graphical component for logical attribute
 * @author Adam Chovanec
 *
 */
public class GUIAttribute extends HBox {

	public final AccessModifierButton btnAccessModifier;

	public final InputTextField txtIdentifier = new InputTextField(GUIUtils.promptTextIdentifier);
	public final InputTextField txtDataType = new InputTextField(GUIUtils.promptTextDataType);
	public final InputTextField txtValue = new InputTextField(GUIUtils.promptTextValue);
	public final Label lblColonSeparator = new Label(":");
	public final Label lblTypeAndValueSeparator = new Label("=");
	public final SettingsButton settingsButton;

	/**
	 * Construct GUI component. Apply access modifier button, settings button, textfields and listeners
	 * DataType textfield reacts on user DnD and highlight text thanks to setRegex method of InputTextField class
	 * @param attribute assigns logical attribute to GUI component
	 * @param guiParent parent of this component - AnchorPane
	 */
	public GUIAttribute(Attribute attribute, Pane guiParent) {
		//initialization
		btnAccessModifier = new AccessModifierButton(
				attribute,
				attribute.getParent().getEntityType().attributeRestrictions()
				);

		settingsButton = new SettingsButton(this)
		.addNonAccessModButton(attribute, attribute.getParent().getEntityType().attributeRestrictions(), txtIdentifier)
		.addSeparator()
		.addMoveButtons(attribute, this, guiParent)
		.addSeparator()
		.addRemoveButton(attribute, this, guiParent)
		.addDebugButtons(attribute);


		//textfields settings
		GUIUtils.asDataTypeTextField(txtDataType);
		GUIUtils.asValueTextField(txtValue);

		txtIdentifier.setText(attribute.identifierProperty().getValue());
		txtDataType.setText(attribute.typeProperty().getValue());
		txtValue.setText(attribute.valueProperty().getValue());

		txtValue.setVisible(false);

		txtIdentifier.focusedProperty().addListener((observable, lostFocus, hasFocus) -> {
			setControlsVisiblity();
		});

		txtDataType.focusedProperty().addListener((observable, lostFocus, hasFocus) -> {
			setControlsVisiblity();
		});

		txtValue.focusedProperty().addListener((observable, lostFocus, hasFocus) -> {
			setControlsVisiblity();
		});
		
		//bindings
		attribute.valueProperty().bind(txtValue.textProperty());
		attribute.identifierProperty().bind(txtIdentifier.textProperty());
		attribute.typeProperty().bind(txtDataType.textProperty());

		//label settings
		lblColonSeparator.setMinWidth(Double.MIN_VALUE);
		lblTypeAndValueSeparator.setMinWidth(Double.MIN_VALUE);
		lblTypeAndValueSeparator.setVisible(false);

		//constrols visiblity
		if(attribute.isInitialized()){
			lblTypeAndValueSeparator.setVisible(true);
			txtValue.setVisible(true);
		}
		
		//events
		setOnMouseEntered(e -> {
			lblTypeAndValueSeparator.setVisible(true);
			txtValue.setVisible(true);
			settingsButton.setVisible(true);
		});

		setOnMouseExited(e -> {
			setControlsVisiblity();
			settingsButton.setVisible(settingsButton.isFocused());
		});

		//layout settings
		setAlignment(Pos.CENTER_LEFT);
		setPadding(new Insets(0, 0, 0, 3));

		HBox.setMargin(settingsButton, new Insets(0, 0, 0, 3));
		getChildren().addAll(
				btnAccessModifier,
				txtIdentifier,
				lblColonSeparator,
				txtDataType,
				lblTypeAndValueSeparator,
				txtValue,
				GUIUtils.regionFiller(),
				settingsButton);
	}

	private void setControlsVisiblity() {
		if (txtValue.getText().length() == 0 && !txtValue.isFocused() && !txtIdentifier.isFocused() && !txtDataType.isFocused()) {
			lblTypeAndValueSeparator.setVisible(false);
			txtValue.setVisible(false);
		}
	}
}
