package cz.jcu.juml.gui.parts;



import cz.jcu.juml.gui.others.GUIManager;
import cz.jcu.juml.gui.others.InputTextArea;
import cz.jcu.juml.gui.others.InputTextField;
import cz.jcu.juml.gui.others.SettingsButton;
import cz.jcu.juml.logic.others.Comment;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Cursor;
import javafx.scene.control.MenuItem;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.Priority;
import javafx.scene.layout.Region;
import javafx.scene.layout.VBox;

/**
 * 
 * Creates graphical component for logical comment
 * @author Adam Chovanec
 *
 */
public class GUIComment extends VBox {

	public final InputTextField txtCommentName = new InputTextField("Nov� pozn�mka");
	public final InputTextArea txtaCommentContent = new InputTextArea("");
	public final SettingsButton settingsButton;
	private double x;
	private double y;
	private boolean isExpanded;
	
	public final HBox headerFullInfo;
	
	private Comment comment;
	/**
	 * Construct GUI component. Apply header and body of the comment as textfields and textarea.
	 * add simple remove button
	 * sets action listeners
	 * manage properties of the component
	 * @param comment assigns logical comment to GUI component
	 * @param guiParent parent of this component - AnchorPane
	 */
	public GUIComment(Comment comment, Pane guiParent) {
		this.comment = comment;
		
		//initialization
		txtCommentName.setText(this.comment.getHeader().getValue());
		txtaCommentContent.setText(this.comment.getContent().getValue());
		//bindings
		this.comment.getHeader().bind(txtCommentName.textProperty());
		this.comment.getContent().bind(txtaCommentContent.textProperty());
		
		//layout settings
		Region region = new Region();
		VBox headerIdAndType = new VBox(txtCommentName);
		HBox.setHgrow(region, Priority.ALWAYS);

		headerIdAndType.setAlignment(Pos.CENTER_LEFT);

		headerFullInfo = new HBox(headerIdAndType, region);	

		getStyleClass().add("entity");
		headerFullInfo.getStyleClass().add("entity-header");
		headerFullInfo.getStyleClass().add("comment");

		txtaCommentContent.getStyleClass().add("comment-textarea");
		
		getChildren().add(headerFullInfo);

		//initialization 2
		settingsButton = new SettingsButton(headerFullInfo);
		
		MenuItem removeButton = new MenuItem("Odstranit");
		removeButton.setOnAction(e -> {
            guiParent.getChildren().remove(this);
            GUIManager.comments.remove(this.comment);
        });
		settingsButton.addCustomButton(removeButton);
		
		//button settings
		HBox.setMargin(settingsButton, new Insets(0, 0, 0, 10));
		headerFullInfo.getChildren().add(settingsButton);

		//events
		headerFullInfo.setOnMousePressed(e -> {
			x = getLayoutX() - e.getSceneX();
			y = getLayoutY() - e.getSceneY();
			setCursor(Cursor.MOVE);
			toFront();
		});

		headerFullInfo.setOnMouseReleased(e -> {
			setCursor(Cursor.HAND);
			settingsButton.setVisible(true);
		});

		headerFullInfo.setOnMouseDragged(e -> {
			if (e.getSceneX() + x < 1 | e.getSceneY() + y < 1) return;
			setLayoutX(e.getSceneX() + x);
			setLayoutY(e.getSceneY() + y);
			settingsButton.setVisible(false);
		});

		headerFullInfo.setOnMouseEntered(e -> {
			setCursor(Cursor.HAND);
			settingsButton.setVisible(true);
		});

		headerFullInfo.setOnMouseEntered(e -> {
			settingsButton.setVisible(true);			
		});

		headerFullInfo.setOnMouseExited(e -> {
			settingsButton.setVisible(settingsButton.isFocused());
		});

		headerFullInfo.getStyleClass().add("entity-header_expanded");
		setOnMouseClicked(e -> {
			requestFocus();
			if(e.getClickCount() == 2){
				setIsExpanded(!isExpanded);
			}
		});
		
		setOnDragOver(e -> {
			e.consume();
		});

		setIsExpanded(true);
	}

	/**
	 * Checks if component is expanded or not
	 * @return true/false
	 */
	public boolean isExpanded(){
		return isExpanded;
	}
	/**
	 * Verification of logical component
	 * 
	 * @return logical comment of this component
	 */
	public Comment getComment(){
		return this.comment;
	}
	
	/*
	public InputTextArea getContentTextArea(){
		return this.txtaCommentContent;
	}
	*/
	/**
	 * Expand or reduce graphical component to show only header or whole comment
	 * Useful for maximizing workspace
	 * @param isExpanded actual status of the component
	 */
	public void setIsExpanded(boolean isExpanded){
		if(this.isExpanded == isExpanded) return;
		this.isExpanded = isExpanded;
		if(isExpanded){ //ne�pln� ov��en�, ale funguje
			getChildren().add(txtaCommentContent);
			headerFullInfo.getStyleClass().remove("entity-header_shortened");
			headerFullInfo.getStyleClass().add("entity-header_expanded");
		} else {
			getChildren().remove(txtaCommentContent);
			headerFullInfo.getStyleClass().remove("entity-header_expanded");
			headerFullInfo.getStyleClass().add("entity-header_shortened");
		}
	}
}
