package cz.jcu.juml.gui.parts;

import cz.jcu.juml.gui.others.GUIUtils;
import cz.jcu.juml.gui.others.InputTextField;
import cz.jcu.juml.gui.others.SettingsButton;
import cz.jcu.juml.logic.javaspec.parts.Parameter;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Label;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;

/**
* Creates graphical component for logical parameter
* @author Adam Chovanec
*
*/
public class GUIParameter extends HBox {

    public final InputTextField txtIdentifier = new InputTextField(GUIUtils.promptTextIdentifier);
    public final InputTextField txtDataType = new InputTextField(GUIUtils.promptTextDataType);
    public final Label lblColonSeparator = new Label(":");
    public final Label lblParameterSeparator = new Label(",");
    public final SettingsButton settingsButton;
    public final Pane guiParent;

    /**
	 * Construct GUI component. Apply access modifier button, settings button, textfields and listeners
	 * @param parameter assigns logical parameter to GUI component
	 * @param guiParent parent of this component - AnchorPane
	 * @param isEnum applies visibility on some components 
	 */
    
    public GUIParameter(Parameter parameter, Pane guiParent, boolean isEnum) {
        //initialization
        this.guiParent = guiParent;
        settingsButton = new SettingsButton(this)
                .addMoveButtons(parameter, this, guiParent)
                .addSeparator()
                .addRemoveButton(parameter, this, guiParent)
                .addDebugButtons(parameter);

        settingsButton.setVisible(true);
        settingsButton.focusedProperty().addListener((observable, lostFocus, hasFocus) -> {
            setControlsVisiblity(isEnum);
            settingsButton.setVisible(true);
        });
        setOnMouseExited(e -> {
            setControlsVisiblity(isEnum);
        });

        if (isEnum==false) {
            
            //textfields settings
            GUIUtils.asDataTypeTextField(txtDataType);
            txtDataType.getStyleClass().add("text-field_bold");
            txtIdentifier.setText(parameter.identifierProperty().getValue());
            txtDataType.setText(parameter.typeProperty().getValue());

            //bindings
            parameter.identifierProperty().bind(txtIdentifier.textProperty());
            parameter.typeProperty().bind(txtDataType.textProperty());

            txtDataType.focusedProperty().addListener((observable, lostFocus, hasFocus) -> {
                if (hasFocus && !getChildren().contains(txtIdentifier)) {//uncompleted check, but works correctly
                    getChildren().remove(lblParameterSeparator);
                    getChildren().addAll(lblColonSeparator, txtIdentifier, settingsButton, lblParameterSeparator);
                } else {
                    setControlsVisiblity(isEnum);
                }
            });

            txtIdentifier.focusedProperty().addListener((observable, lostFocus, hasFocus) -> {
                setControlsVisiblity(isEnum);
            });

            //label settings
            lblColonSeparator.setMinWidth(Double.MIN_VALUE);

            //layout settings
            setAlignment(Pos.CENTER_LEFT);
            setPadding(new Insets(0, 0, 0, 3));
            getChildren().addAll(txtDataType, lblParameterSeparator);

            txtDataType.requestFocus();
        } else {
            txtIdentifier.setText(parameter.identifierProperty().getValue());
            txtIdentifier.setPromptText("hodnota");
            //bindings
            parameter.identifierProperty().bind(txtIdentifier.textProperty());
            
            txtIdentifier.focusedProperty().addListener((observable, lostFocus, hasFocus) -> {//exception
                if (hasFocus && !getChildren().contains(txtIdentifier)) {//uncompleted check, but works correctly
                    getChildren().remove(lblParameterSeparator);
                    getChildren().addAll(settingsButton);
                } else {
                    setControlsVisiblity(isEnum);
                }
            });
            
            //layout settings
            setAlignment(Pos.CENTER_LEFT);
            setPadding(new Insets(0, 0, 0, 3));
            getChildren().addAll(txtIdentifier,lblParameterSeparator);
            txtIdentifier.requestFocus();
        }

    }

    private void setControlsVisiblity(boolean isEnum) {
        if (!txtDataType.isFocused() && !txtIdentifier.isFocused() && !settingsButton.isFocused() && isEnum==false) {/* && !settingsMenuButton.isShowing()*/ 
            getChildren().removeAll(lblColonSeparator, txtIdentifier, settingsButton);
        }
        /*else{
            getChildren().removeAll(lblColonSeparator,settingsButton);
        }

        /*if(guiParent.getChildren().size() == 1 || guiParent.getChildren().indexOf(this) == guiParent.getChildren().size() - 1)
         getChildren().remove(lblParameterSeparator);
         else
         getChildren().add(lblParameterSeparator);*/
    }
}
