package cz.jcu.juml.gui.parts;

import cz.jcu.juml.logic.javaspec.parts.Method;
import cz.jcu.juml.logic.javaspec.parts.Parameter;
import cz.jcu.juml.logic.others.EntityManager;
import javafx.beans.property.SimpleStringProperty;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.Tooltip;
import javafx.scene.layout.HBox;

/**
 * Extension of GUIParameter to define minor aspects of parameter syntax.
 * Adds container of parameters as HBox to be able to create more parameters
 * @author Adam Chovanec
 *
 */
public class GUIParameterBox extends HBox {

	public final Label leftBracket = new Label("(");
	public final Label rightBracket = new Label(")");
	public final Button btnAddParameter = new Button("+");
	public final HBox parameters = new HBox();
/**
 * Registering parameter box to logical method
 * Apply addParameter button
 * Setting actions
 * 
 * @param method to apply this parameter box
 * @param isEnum applies visibility on some components 
 */
	public GUIParameterBox(Method method, boolean isEnum){
		//button settings		
		btnAddParameter.setTooltip(new Tooltip("P�idat parametr"));
		btnAddParameter.getStyleClass().addAll("rounded_transparent_button");
		btnAddParameter.setVisible(false);
		
		//events
		btnAddParameter.setOnAction(e -> {
			Parameter parameter = new Parameter(new SimpleStringProperty(""), new SimpleStringProperty(""), false, method);
			EntityManager.add(parameter);
			GUIParameter guiParameter = new GUIParameter(parameter, parameters,isEnum);
			parameters.getChildren().add(guiParameter);
			guiParameter.requestFocus();
		});

		btnAddParameter.setOnMouseEntered(e -> {
			getStyleClass().add("element_adding");
		});

		btnAddParameter.setOnMouseExited(e -> {
			getStyleClass().remove("element_adding");
		});

		setOnMouseEntered(e -> {
			btnAddParameter.setVisible(true); 
		});

		setOnMouseExited(e -> {
			btnAddParameter.setVisible(false);
		});

		//layout settings
		setAlignment(Pos.CENTER_LEFT);
		getChildren().addAll(leftBracket, parameters, rightBracket, btnAddParameter);

        
        //check method parameters
        for(Parameter parameter : method.getParameters()){
			GUIParameter guiParameter = new GUIParameter(parameter, parameters, isEnum);
			parameters.getChildren().add(guiParameter);
			guiParameter.requestFocus();
        }
	}
}
