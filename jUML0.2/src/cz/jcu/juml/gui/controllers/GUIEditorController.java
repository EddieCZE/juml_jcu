/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.jcu.juml.gui.controllers;

import java.net.URL;
import java.util.ResourceBundle;

import org.fxmisc.richtext.LineNumberFactory;

import cz.jcu.juml.gui.others.GUIManager;
import cz.jcu.juml.gui.others.JavaCodeEditor;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.layout.AnchorPane;

/**
 * FXML Controller class for creating JavaCodeEditor for class to show whole source code of the class
 *
 * @author Adam Chovanec
 */
public class GUIEditorController extends AnchorPane implements Initializable {

    /**
     * Initializes the controller class.
     */
    
     @FXML
     private AnchorPane editorPane;
     
    //public JavaCodeEditor javaCodeEditor0 = new JavaCodeEditor("//koment�� metody");
    public static JavaCodeEditor javaCodeEditor;

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        javaCodeEditor = new JavaCodeEditor("");
        javaCodeEditor.setParagraphGraphicFactory(LineNumberFactory.get(javaCodeEditor));
        javaCodeEditor.setEditable(false);
        //editorPane.getChildren().add(javaCodeEditor);
        editorPane.getChildren().add(javaCodeEditor);
        GUIManager.registerGUIEditorController(this);
    }

}
