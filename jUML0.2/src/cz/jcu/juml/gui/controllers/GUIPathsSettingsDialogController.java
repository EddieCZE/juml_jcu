package cz.jcu.juml.gui.controllers;

import java.io.File;
import java.net.URL;
import java.util.ResourceBundle;
import cz.jcu.juml.gui.others.GUIManager;
import cz.jcu.juml.logic.others.io.Compilerr;
import cz.jcu.juml.logic.others.io.IOUtils;
import javafx.animation.FadeTransition;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.VBox;
import javafx.stage.DirectoryChooser;
import javafx.util.Duration;
/**
 * Controller responsible for creating path setting window
 * 
 * @author Eddie
 *
 */
public class GUIPathsSettingsDialogController extends VBox implements Initializable {

    @FXML private TextField txtCompilerPath;
    @FXML private TextField txtWorkspacePath;
    
    @FXML private Button btnWorkspacePath;
    @FXML private Button btnCompilerPath;
    @FXML private Button btnPathSettingsDialogOK;
    
    @FXML private Label lblCompilerPath;
    @FXML private Label lblCompilerDetection;
    
	private File lastDirectory;
	private File compilerDirectory = new File("C:\\Program Files\\Java\\");
	private DirectoryChooser dirChooser = new DirectoryChooser();

	public static SimpleBooleanProperty compilerDetected = new SimpleBooleanProperty();
	/**
	 * Overwritten method initialize that provides path to workspace and the latest JDK
	 * If workspace folder isn't chosen this method creates a default folder C:\\Projects 
	 */
	@Override
	public void initialize(URL arg0, ResourceBundle arg1) {

		lblCompilerDetection.setStyle("-fx-background-color: red; -fx-text-fill: white;");
		lblCompilerDetection.setText(" Kompil�tor nenalezen. ");
		
		final FadeTransition fadeTransition = new FadeTransition();
		fadeTransition.setNode(lblCompilerDetection);
		fadeTransition.setDuration(Duration.millis(1000));
		fadeTransition.setFromValue(0);
		fadeTransition.setToValue(1);
				
		btnWorkspacePath.setOnAction(e -> {
			if(lastDirectory != null)
				dirChooser.setInitialDirectory(lastDirectory);
			else	
                            dirChooser.setInitialDirectory(new File("C:\\")); //possible danger
			File directory = dirChooser.showDialog(GUIManager.pathsSettingsDialog);
			if(directory != null){
				txtWorkspacePath.setText(directory.getAbsolutePath());
				IOUtils.workspacePath = txtWorkspacePath.getText();
				lastDirectory = directory;
			}
                        // Pokud se nevybere pracovn� adres�� je vytvo�en defaultn� na C:\ pod n�zvem Projects
                        else{
                            directory=IOUtils.createDirectory("C:\\Projects\\");
                            txtWorkspacePath.setText(directory.getAbsolutePath());
                            IOUtils.workspacePath = txtWorkspacePath.getText();
                            lastDirectory = directory;
                            
                        }
		});

		btnCompilerPath.setOnAction(e -> {
			if(compilerDirectory.isDirectory())
				dirChooser.setInitialDirectory(compilerDirectory);
			File directory = dirChooser.showDialog(GUIManager.pathsSettingsDialog);
			if(directory != null){
				txtCompilerPath.setText(directory.getAbsolutePath());
				IOUtils.compilerPath = txtCompilerPath.getText();
				try {
					Compilerr.getCompiler();
					lblCompilerDetection.setStyle("-fx-background-color: greenyellow; -fx-text-fill: black;");
					lblCompilerDetection.setText(" Kompil�tor �sp�n� detekov�n. ");
					lblCompilerPath.setDisable(true);
					txtCompilerPath.setDisable(true);
					btnCompilerPath.setDisable(true);
					compilerDetected.setValue(true);
				} catch (Exception e1) {
					lblCompilerDetection.setStyle("-fx-background-color: red; -fx-text-fill: white;");
					lblCompilerDetection.setText(" Kompil�tor nenalezen. ");
					compilerDetected.setValue(false);
				}
				fadeTransition.play();
			}
		});
		
		btnPathSettingsDialogOK.setOnAction(e -> {
			GUIManager.pathsSettingsDialog.close();
			GUIManager.guiController.mainBorderPane.setEffect(null);
		});
		
	}
}
