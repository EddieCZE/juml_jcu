package cz.jcu.juml.logic.javaspec.parts;

import java.io.Serializable;
import java.util.List;

import cz.jcu.juml.logic.javaspec.AccessModifier;
import cz.jcu.juml.logic.javaspec.NonAccessModifier;
import javafx.beans.property.SimpleStringProperty;

/**
 * This abstract class represents individual members of a class (enums, attributes, constructors, methods)
 * 
 * @author Adam Chovanec
 *
 */
public abstract class Member extends Element implements Serializable {

	private static final long serialVersionUID = 1L;
	
	transient SimpleStringProperty type;
    Entity parent;

    public Member(AccessModifier accessMod, List<NonAccessModifier> nonAccessMods, SimpleStringProperty identifier, SimpleStringProperty type, Entity parent) {
        super(accessMod, nonAccessMods, identifier);
        this.type = type;
        this.parent = parent;
    }   

    public SimpleStringProperty typeProperty() {
        return type;
    }

    public Entity getParent() {
        return parent;
    }
    
    public void setType(SimpleStringProperty type) {
        this.type = type;
    }

    public void setParent(Entity parent) {
        this.parent = parent;
    }

    @Override
    public String toString() {
        return super.toString() + ", data/return type: " + type.getValue() + ", parentDetected: " + (parent != null);
    }

    @Override
    public boolean equals(Object o) {
        if (o == null) {
            return false;
        }
        if (o == this) {
            return true;
        }
        if (!(o instanceof Member)) {
            return false;
        }
        Member obj = (Member) o;
        return super.equals(obj)
                && this.type.equals(obj.type)
                && this.parent.equals(obj.parent);
    }
}
