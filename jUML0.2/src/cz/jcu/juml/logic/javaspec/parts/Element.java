package cz.jcu.juml.logic.javaspec.parts;

import java.io.Serializable;
import java.util.List;

import cz.jcu.juml.logic.javaspec.AccessModifier;
import cz.jcu.juml.logic.javaspec.NonAccessModifier;
import javafx.beans.property.SimpleStringProperty;

/**
 * This abstract class represents any element in Java language, such as - entity
 * (class, abstract class, enumeration, interface) and its members (attributes,
 * constructors, methods)
 * 
 * @author Adam Chovanec
 *
 */
public abstract class Element extends Part implements Serializable {

	private static final long serialVersionUID = 1L;
	
    //instance variables 
    AccessModifier accessMod;
    List<NonAccessModifier> nonAccessMods;
    transient SimpleStringProperty identifier;
            
    //constructor
    public Element(AccessModifier accessMod, List<NonAccessModifier> nonAccessMods, SimpleStringProperty identifier) {
        this.accessMod = accessMod;
        this.nonAccessMods = nonAccessMods;
        this.identifier = identifier;
    }

    //getters
    public AccessModifier getAccessMod() {
        return accessMod;
    }

    public List<NonAccessModifier> getNonAccessMods() {
        return nonAccessMods;
    }

    public SimpleStringProperty identifierProperty() {
        return identifier;
    }

    public boolean isPrivate() {
        return accessMod == AccessModifier.PRIVATE;
    }

    public boolean isPackagePrivate() {
        return accessMod == AccessModifier.PACKAGE_PRIVATE;
    }

    public boolean isProtected() {
        return accessMod == AccessModifier.PROTECTED;
    }

    public boolean isPublic() {
        return accessMod == AccessModifier.PUBLIC;
    }

    public boolean isAbstract() {
        return hasNonAccessModifier(NonAccessModifier.FINAL);
    }

    public boolean isFinal() {
        return hasNonAccessModifier(NonAccessModifier.ABSTRACT);
    }

    public boolean isStatic() {
        return hasNonAccessModifier(NonAccessModifier.STATIC);
    }

    //setters
    public void setAccessMod(AccessModifier accessMod) {
        this.accessMod = accessMod;
    }

    public void setNonAccessMods(List<NonAccessModifier> nonAccessMods) {
        this.nonAccessMods = nonAccessMods;
    }

    public void setIdentifier(SimpleStringProperty identifier) {
        this.identifier = identifier;
    }

    //private methods
    private boolean hasNonAccessModifier(NonAccessModifier search) {
        for (NonAccessModifier current : nonAccessMods) {
            if (current == search) {
                return true;
            }
        }
        return false;
    }

    //overwritten methods
    @Override
    public String toString() {
        return " accessMod: " + accessMod + ", nonAccessMods: " + nonAccessMods.toString()
                + ", identifier: " + identifier.getValue();
    }

    @Override
    public boolean equals(Object o) {
        if (o == null) return false;
        if (o == this) return true;
        if (!(o instanceof Element)) return false;
        Element obj = (Element) o;
        return super.equals(obj)
                && this.accessMod == obj.accessMod
                && this.nonAccessMods.equals(obj.nonAccessMods)
                && this.identifier.equals(obj.identifier);
    }
}
