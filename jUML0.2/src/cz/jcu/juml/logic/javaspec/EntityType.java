package cz.jcu.juml.logic.javaspec;

import cz.jcu.juml.logic.others.Codeable;
import cz.jcu.juml.logic.others.Describable;
import cz.jcu.juml.logic.others.LogicUtils;
/**
 * EntityType determines basic kinds of classes.
 * Each enum consists of code, description and four restrictions
 * 
 * @author Adam Chovanec
 *
 */
public enum EntityType implements Codeable, Describable {

    CLASS("class", "T��da",
            new Restrictions(//entity restrictions
                    LogicUtils.newList(AccessModifier.PACKAGE_PRIVATE, AccessModifier.PUBLIC),
                    LogicUtils.newList(NonAccessModifier.NONE, NonAccessModifier.FINAL)),
            new Restrictions(//attribute restrictions
                    LogicUtils.newList(AccessModifier.values()),
                    LogicUtils.newList(NonAccessModifier.NONE, NonAccessModifier.STATIC, NonAccessModifier.FINAL)),
            new Restrictions(//constructor restrictions
                    LogicUtils.newList(AccessModifier.values()),
                    LogicUtils.newList(NonAccessModifier.NONE)),
            new Restrictions(//method restrictions
                    LogicUtils.newList(AccessModifier.values()),
                    LogicUtils.newList(NonAccessModifier.NONE, NonAccessModifier.STATIC, NonAccessModifier.FINAL))
    ),
    ABSTRACT_CLASS("class", "Abstraktn� t��da",
            new Restrictions(//entity restrictions
                    LogicUtils.newList(AccessModifier.PACKAGE_PRIVATE, AccessModifier.PUBLIC),
                    LogicUtils.newList(NonAccessModifier.ABSTRACT)),
            new Restrictions(///attribute restrictions
                    LogicUtils.newList(AccessModifier.values()),
                    LogicUtils.newList(NonAccessModifier.NONE, NonAccessModifier.STATIC, NonAccessModifier.FINAL)),
            new Restrictions(//constructor restrictions
                    LogicUtils.newList(AccessModifier.values()),
                    LogicUtils.newList(NonAccessModifier.NONE)),
            new Restrictions(//method restrictions
                    LogicUtils.newList(AccessModifier.values()),
                    LogicUtils.newList(NonAccessModifier.values()))
    ),
    ENUMERATION("enum", "V��tov� t��da",
            new Restrictions(//entity restrictions
                    LogicUtils.newList(AccessModifier.PACKAGE_PRIVATE, AccessModifier.PUBLIC),
                    LogicUtils.newList(NonAccessModifier.NONE)),
            new Restrictions(//attribute restrictions
                    LogicUtils.newList(AccessModifier.values()),
                    LogicUtils.newList(NonAccessModifier.NONE, NonAccessModifier.STATIC, NonAccessModifier.FINAL)),
            new Restrictions(//constructor restrictions
                    LogicUtils.newList(AccessModifier.PRIVATE),
                    LogicUtils.newList(NonAccessModifier.NONE)),
            new Restrictions(//method restrictions
                    LogicUtils.newList(AccessModifier.values()),
                    LogicUtils.newList(NonAccessModifier.values()))
    ),
    INTERFACE("interface", "Rozhran�",
            new Restrictions(//entity restrictions
                    LogicUtils.newList(AccessModifier.PACKAGE_PRIVATE, AccessModifier.PUBLIC),
                    LogicUtils.newList(NonAccessModifier.ABSTRACT)),
            new Restrictions(//attribute restrictions
                    LogicUtils.newList(AccessModifier.PACKAGE_PRIVATE, AccessModifier.PUBLIC),
                    LogicUtils.newList(NonAccessModifier.STATIC, NonAccessModifier.FINAL)),
            null, //constructor restrictions
            new Restrictions(//method restrictions
                    LogicUtils.newList(AccessModifier.PACKAGE_PRIVATE, AccessModifier.PUBLIC),
                    LogicUtils.newList(NonAccessModifier.ABSTRACT))
    );

    private final String code;
    private final String description;

    private final Restrictions entityRestrictions;
    private final Restrictions attributeRestrictions;
    private final Restrictions constructorRestrictions;
    private final Restrictions methodRestrictions;

    private EntityType(String code, String description, Restrictions entityRestrictions, Restrictions attributeRestrictions, Restrictions constructorRestrictions, Restrictions methodRestrictions) {
        this.code = code;
        this.description = description;
        this.entityRestrictions = entityRestrictions;
        this.attributeRestrictions = attributeRestrictions;
        this.methodRestrictions = methodRestrictions;
        this.constructorRestrictions = constructorRestrictions;
    }

    /**
     * Offers information about entity restrictions
     * @return entity restrictions
     */
    public Restrictions entityRestrictions() {
        return entityRestrictions;
    }

    /**
     * Offers information about attribute restrictions
     * @return attribute restrictions
     */
    public Restrictions attributeRestrictions() {
        return attributeRestrictions;
    }

    /**
     * Offers information about method restrictions
     * @return method restrictions
     */
    public Restrictions methodRestrictions() {
        return methodRestrictions;
    }

    /**
     * Offers information about constructor restrictions
     * @return constructor restrictions
     */
    public Restrictions constructorRestrictions() {
        return constructorRestrictions;
    }

    @Override
    public String toJavaCode() {
        return code;
    }

    @Override
    public String getDescription() {
        return description;
    }
}
