package cz.jcu.juml.logic.others.io;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.List;

import cz.jcu.juml.gui.others.GUIManager;
import cz.jcu.juml.gui.parts.GUIComment;
import cz.jcu.juml.gui.parts.GUIEntity;
import cz.jcu.juml.logic.javaspec.parts.Attribute;
import cz.jcu.juml.logic.javaspec.parts.Entity;
import cz.jcu.juml.logic.javaspec.parts.Method;
import cz.jcu.juml.logic.javaspec.parts.Parameter;
import cz.jcu.juml.logic.others.Comment;
import cz.jcu.juml.logic.others.EntityManager;
import javafx.beans.property.SimpleStringProperty;

public class Serializer {
	
	public static void serializeProject(File file) throws IOException{
		FileOutputStream fileOut = new FileOutputStream(file);
		ObjectOutputStream objOut = new ObjectOutputStream(fileOut);
		
		List<Comment> comments = GUIManager.comments;
		objOut.writeInt(comments.size());
		for(Comment comment : comments){
			objOut.writeObject(comment);
			
			objOut.writeObject(comment.getHeader().getValue());
			objOut.writeObject(comment.getContent().getValue());
			
			GUIComment guiComment = GUIManager.getGUIComment(comment);
			objOut.writeDouble(guiComment.getLayoutX());			
			objOut.writeDouble(guiComment.getLayoutY());
			objOut.writeBoolean(guiComment.isExpanded());
		}
		
		List<Entity> entities = EntityManager.entities;
		objOut.writeInt(entities.size());
		for(Entity entity : entities){
			objOut.writeObject(entity);
			
			//CUSTOMFIX SERIALIZATION
			//save entity informations
			objOut.writeObject(entity.identifierProperty().getValue());

			//save entity members
			List<Attribute> attributes = entity.getAttributes();
			for(Attribute attribute : attributes){
				objOut.writeObject(attribute.identifierProperty().getValue());
				objOut.writeObject(attribute.typeProperty().getValue());
				objOut.writeObject(attribute.valueProperty().getValue());
			}
			
			List<Method> constructors = entity.getConstructors();
			for(Method constructor : constructors){
				objOut.writeObject(constructor.identifierProperty().getValue());
				objOut.writeObject(constructor.typeProperty().getValue());
                                objOut.writeObject(constructor.bodyProperty().getValue());
                                objOut.writeObject(constructor.commentProperty().getValue());
				List<Parameter> parameters = constructor.getParameters();
				for(Parameter parameter : parameters){
					objOut.writeObject(parameter.identifierProperty().getValue());
					objOut.writeObject(parameter.typeProperty().getValue());
				}
			}
			
			List<Method> methods = entity.getMethods();
			for(Method method : methods){
				objOut.writeObject(method.identifierProperty().getValue());
				objOut.writeObject(method.typeProperty().getValue());
                                objOut.writeObject(method.bodyProperty().getValue());
                                objOut.writeObject(method.commentProperty().getValue());
				List<Parameter> parameters = method.getParameters();
				for(Parameter parameter : parameters){
					objOut.writeObject(parameter.identifierProperty().getValue());
					objOut.writeObject(parameter.typeProperty().getValue());
				}
			}
                        
			List<Method> enums = entity.getEnums();
			for(Method enumeration : enums){
				objOut.writeObject(enumeration.identifierProperty().getValue());
				objOut.writeObject(enumeration.typeProperty().getValue());
                                objOut.writeObject(enumeration.bodyProperty().getValue());
                                objOut.writeObject(enumeration.commentProperty().getValue());
				List<Parameter> parameters = enumeration.getParameters();
				for(Parameter parameter : parameters){
					objOut.writeObject(parameter.identifierProperty().getValue());
					objOut.writeObject(parameter.typeProperty().getValue());
				}
			}
                        
			//gui serialization
			GUIEntity guiEntity = GUIManager.getGUIEntity(entity);
			objOut.writeDouble(guiEntity.getLayoutX());			
			objOut.writeDouble(guiEntity.getLayoutY());
			objOut.writeBoolean(guiEntity.isExpanded());
		}
		objOut.close();
		fileOut.close();
	}

	public static void deserializeProject(File file) throws IOException, ClassNotFoundException{
		FileInputStream fileIn = new FileInputStream(file);
		ObjectInputStream objIn = new ObjectInputStream(fileIn);
		
		int size = objIn.readInt();
		for(int i = 0; i < size; i++){
			Comment comment = (Comment) objIn.readObject();
			comment.setHeader(new SimpleStringProperty((String)objIn.readObject()));
			comment.setContent(new SimpleStringProperty((String)objIn.readObject()));
			
			GUIManager.comments.add(comment);
			GUIComment guiComment = GUIManager.createGUIComment(comment);
			guiComment.setLayoutX(objIn.readDouble());
			guiComment.setLayoutY(objIn.readDouble());
			guiComment.setIsExpanded(objIn.readBoolean());
		}
		
		size = objIn.readInt();
		for(int i = 0; i < size; i++){
			Entity entity = (Entity) objIn.readObject();
			entity.setIdentifier(new SimpleStringProperty((String)objIn.readObject()));
				
			List<Attribute> attributes = entity.getAttributes();
			for(Attribute attribute : attributes){
				attribute.setIdentifier(new SimpleStringProperty((String)objIn.readObject()));
				attribute.setType(new SimpleStringProperty((String)objIn.readObject()));
				attribute.setValue(new SimpleStringProperty((String)objIn.readObject()));
			}
			
			List<Method> constructors = entity.getConstructors();			
			for(Method constructor : constructors){				
				constructor.setIdentifier(new SimpleStringProperty((String)objIn.readObject()));
				constructor.setType(new SimpleStringProperty((String)objIn.readObject()));                                
                                constructor.setBody(new SimpleStringProperty((String)objIn.readObject()));
                                constructor.setComment(new SimpleStringProperty((String)objIn.readObject()));
				List<Parameter> parameters = constructor.getParameters();
				for(Parameter parameter : parameters){
					parameter.setIdentifier(new SimpleStringProperty((String)objIn.readObject()));
					parameter.setType(new SimpleStringProperty((String)objIn.readObject()));
				}
			}
			
			List<Method> methods = entity.getMethods();
			for(Method method : methods){
				method.setIdentifier(new SimpleStringProperty((String)objIn.readObject()));
				method.setType(new SimpleStringProperty((String)objIn.readObject()));
                                method.setBody(new SimpleStringProperty((String)objIn.readObject()));
                                method.setComment(new SimpleStringProperty((String)objIn.readObject()));
				List<Parameter> parameters = method.getParameters();
				for(Parameter parameter : parameters){
					parameter.setIdentifier(new SimpleStringProperty((String)objIn.readObject()));
					parameter.setType(new SimpleStringProperty((String)objIn.readObject()));
				}
			}
                        
                        List<Method> enums = entity.getEnums();
			for(Method enumeration : enums){
				enumeration.setIdentifier(new SimpleStringProperty((String)objIn.readObject()));
				enumeration.setType(new SimpleStringProperty((String)objIn.readObject()));
                                enumeration.setBody(new SimpleStringProperty((String)objIn.readObject()));
                                enumeration.setComment(new SimpleStringProperty((String)objIn.readObject()));
				List<Parameter> parameters = enumeration.getParameters();
				for(Parameter parameter : parameters){
					parameter.setIdentifier(new SimpleStringProperty((String)objIn.readObject()));
					parameter.setType(new SimpleStringProperty((String)objIn.readObject()));
				}
			}
			
			EntityManager.entities.add(entity);
			GUIEntity guiEntity = GUIManager.createGUIEntity(entity);
			guiEntity.setLayoutX(objIn.readDouble());
			guiEntity.setLayoutY(objIn.readDouble());
			guiEntity.setIsExpanded(objIn.readBoolean());
		}
		objIn.close();
		fileIn.close();
	}
}
