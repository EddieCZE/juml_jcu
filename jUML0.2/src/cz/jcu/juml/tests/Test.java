package cz.jcu.juml.tests;

import java.io.File;
import java.io.IOException;

import cz.jcu.juml.logic.others.io.JarExporter;

public class Test {
	public static void main(String[] args) throws IOException {
		File archiveFile = new File("E:\\eddiebp\\myapp.jar");
		archiveFile.createNewFile();
		File[] filesToArchive = new File[]{
				new File("E:\\eddiebp\\Entita1.class"),
				new File("E:\\eddiebp\\EntitaX1.class"),
				new File("E:\\eddiebp\\Tralaaaaa.class"),
				new File("E:\\eddiebp\\XXX.class"),
				new File("E:\\eddiebp\\MainTrida.class")
		};
		JarExporter.createJarArchive(archiveFile, filesToArchive, "MainTrida");
	}
}
